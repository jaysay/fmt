<?php

require_once('Model.php');

class Settings extends Model
{
    protected $table;

    protected $primaryKey = 'id';

    protected $client;

    public function __construct($table, $client)
    {
        $this->table = $table;
        $this->client = $client;
    }
}
