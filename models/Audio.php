<?php

require_once('Model.php');

class Audio extends Model
{
    protected $primaryKey = "audio_id";

    protected $table;

    protected $client;
    
    public function __construct($table, $client)
    {
        $this->table = $table;
    
        $this->client = $client; 
    }
}
