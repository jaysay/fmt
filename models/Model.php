<?php

class Model
{
    protected $table;

    protected $primaryKey;

    protected $fillable;

    protected $client;

    public function all($user_id)
    {
        $query = "SELECT * FROM " . $this->table;
        if ($user_id) {
            $query .= " WHERE user_id = " . $user_id;
        }
        return $this->client->get_results($query);
    }
    
    public function create($data = [])
    {
        $result = $this->client->insert($this->table, $data);
        $query = "SELECT * FROM " . $this->table . " WHERE " . $this->primaryKey . " = LAST_INSERT_ID()";
        
        return $this->client->get_results($query);
    }

    public function find($id)
    {
        $query = "SELECT * FROM " . $this->table . " WHERE " . $this->primaryKey . " = " . $id;
        return $this->client->get_results($query);
    }

    public function delete($id)
    {
        $query = "DELETE FROM " . $this->table . " WHERE " . $this->primaryKey . " = " . $id;
        return $this->client->get_results($query);
    }

    public function update($id, $data)
    {
        $sets = "";

        foreach ($data as $field => $value) {
            $sets .= $field . " = '" . $value . "',";
        }

        $query = "UPDATE " . $this->table . " SET " . trim($sets, ',') . " WHERE " . $this->primaryKey . " = " . $id;

        return $this->client->get_results($query);
    }
}
