<?php

require_once('Model.php');

class Alert extends Model
{
    protected $primaryKey = "alert_id";

    protected $table;

    protected $client;
    
    public function __construct($table, $client)
    {
        $this->table = $table;
    
        $this->client = $client; 
    }
}
