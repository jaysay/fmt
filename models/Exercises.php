<?php

require_once('Model.php');

class Exercises extends Model
{
    protected $primaryKey = "id";

    protected $table;

    protected $client;
    
    public function __construct($table, $client)
    {
        $this->table = $table;
    
        $this->client = $client; 
    }
}
