<?php

require_once('Model.php');

class PackageExercise extends Model
{
    protected $table;

    protected $primaryKey = 'id';

    protected $client;

    public function __construct($table, $client)
    {
        $this->table = $table;
        $this->client = $client;
    }

    public function cdelete($data)
    {
        $condition = "";
        
        foreach ($data as $key => $value) {
            $condition .= $key . " = " . $value . " AND ";
        }

        $query = "DELETE FROM " . $this->table . " WHERE " . rtrim($condition, " AND ");
        return $this->client->get_results($query);
    }
}
