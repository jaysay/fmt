<div class="container">
    <div class="clearfix">
        <div class="col-md-8 col-md-offset-2">
            <h1>FM Race Pacer</h1>
            <h2>Create Package</h2>

            <div class="form-field">
                <br>
                <label>Package Name</label>
                <input type="text" id="package-name" class="form-control" placeholder="Package name...">
            </div>

            <div class="form-field">
                <br>
                <label>Price</label>
                <input type="text" id="package-price" class="form-control" placeholder="$100">
            </div>

            <div class="form-field">
                <br>
                <label>Description</label>
                <textarea id="package-description" class="form-control" rows="10" placeholder="Package description..."></textarea>
            </div>

            <div class="form-field">
                <br>
                <button class="button" onClick="document.getElementById('package-exer-modal').style.display='block';">Select Exercises</button>
            </div>

            <div class="form-field">
                <br>
                <button id="create-package" class="button button-primary">Create Package</button>
            </div>
        </div>
    </div>
</div>
<div id="package-exer-modal" style="display: none; background: #f1f1f1; position: absolute; left: 0; right: 0; top: 0; bottom: 0; padding-right: 20px;">
    <div style="display: table; height: 100%;">
        <div style="display: table-cell; vertical-align: middle;">
            <table class="wp-list-table widefat fixed striped posts">
                <thead>
                    <tr>
                        <th scope="col"  style="width: 15px;"></th>
                        <th scope="col" style="width: 15px; text-align: center">#</th>
                        <th scope="col">Exercise Name</th>
                        <th scope="col">Category Name</th>
                    </tr>
                </thead>

                <tbody id="package-exer-list"></tbody>

                <tfoot>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col" style="text-align: center">#</th>
                        <th scope="col">Exercise Name</th>
                        <th scope="col">Category Name</th>
                    </tr>
                </tfoot>
            </table>
            <br>
            <button id="select-package-exer" class="button-primary">Select</button>
            <button class="button" onClick="document.getElementById('package-exer-modal').style.display='none';">Cancel</button>
        </div>
    </div>
</div>
