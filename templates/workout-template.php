<style>
    [data-status="open"] { display: block; }
    [data-status="close"] { display: none; }
    #add-exercise-modal,
    #add-workout-modal { position: fixed; background: white; top: 0; left: 0; right: 0; bottom: 0; margin-top: 10px; }
</style>
<div class="temp-fix-wrap" ng-app="fmt-app" ng-controller="WorkoutCtrl" ng-cloak>
    <div>
        <div>
        <!-- exercise table -->
        <div class="workout-container" ng-hide="select_workout" ng-cloak>
            <audio id="audio" class="hidden" controls>
                <source src="<?php echo plugins_url('fmt/media/cyclerate.mp3'); ?>" type="audio/ogg"></source>
                <source src="<?php echo plugins_url('fmt/media/cyclerate.mp3'); ?>" type="audio/mpeg"></source>
            </audio>
            <audio id="set" class="hidden" controls>
                <source src="<?php echo plugins_url('fmt/media/set_complete.mp3'); ?>" type="audio/ogg"></source>
                <source src="<?php echo plugins_url('fmt/media/set_complete.mp3'); ?>" type="audio/mpeg"></source>
            </audio>
        </div>
        <!-- end exercise table -->
        
        <!-- Workout List Mobile Style -->
        <div id="workoutlist-mobile" ng-show="select_workout">
            <div class="fmt-add-btn">
                <button data-toggle="modal" ng-click="showCreateWorkout()">Create Workout</button>
                <?php global $current_user; ?>
                <?php if ($current_user->caps['administrator']) : ?>
                <button data-toggle="modal" data-target="#workoutcategory" ng-click="createWorkoutCategory()">Workout Categories</button><br><br>
                <?php endif; ?>
                <label for="">Sort by: </label>
                <select ng-model="sortby">
                    <option value="name">Name</option>
                    <option value="category">Category</option>
                    <option value="created_at">Created Time</option>
                </select>
            </div>
            <div class="fmt-list-container">
                <div class="fmt-search-list">
                    <input type="text" placeholder="search..." ng-model="workoutsearch">
                </div>
                <div class="list-items">
                    <div class="fmt-list-item" ng-repeat="workout in workouts track by $index" ng-click="setup($index)" ng-cloak>
                        <div>
                            <p style="margin:0;padding:0;" class="text-capitalize" ng-cloak>{{ workout.name }}</p>
                            <small ng-cloak>{{ workout.created_at | time }}</small>
                        </div>
                        <div ng-cloak>{{ workout.category }}</div>
                        <div ng-click="$event.stopPropagation()">
                            <button ng-click="editworkout($index)" ng-cloak><i class="fa fa-pencil"></i></button>
                            <button ng-click="removeWorkout($index)" ng-cloak><i class="fa fa-remove"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Workout List Mobile Style End -->
        
        <!-- Exercise Table -->
        <div id="routine-list" ng-hide="select_workout">
            <button class="btn btn-info btn-sm" ng-click="addRoutine()">Add Exercise</button>
            <button class="btn btn-success btn-sm" ng-click="begin_training()" data-toggle="modal" data-target="#myModal">Start Exercise</button>
            <button class="btn btn-default btn-sm pull-right"><i class="fa fa-envelope"></i></button>
            <button class="btn btn-default btn-sm pull-right" ng-click="print()"><i class="fa fa-print"></i></button>
            <button class="btn btn-default btn-sm pull-right" ng-click="selectWorkout()">Workout Lists</button><br><br>
            <input type="text" placeholder="search exercise" ng-model="exercise_search" />
            <div id="accordion">
                <div ng-show="workout_routine.length <= 0 ">
                    <br><br>
                    <h1 class="text-center">No exercise listed</h1>
                </div>
                <div ng-hide="workout_routine.length <= 0 " class="accordion-item" ng-repeat="routine in workout_routine track by $index" ng-cloak>
                    <input type="checkbox" class="selector" id="check-{{ $index }}"/>
                    <label for="check-{{ $index }}" ng-class="{ 'selected': routine.include }">
                        <span ng-click="$event.stopPropagate()">
                            <input type="checkbox" ng-click="include($index)" ng-model="routine.included" />
                        </span>
                        <span>
                            <small ng-show="routine.exercise">{{ routine.exercise.name }}</small>
                            <small ng-hide="routine.exercise">Edit to select exercise</small>
                        </span>
                        <span>
                            <a href="javascript:void(0)" ng-click="animationLink($index)" ng-show="routine.exercise.animation_link">
                                <img src="{{ routine.exercise.thumbnail }}" alt="animation" width="40" height="40" />
                            </a>
                        </span>
                        <span class="btn-group btn-right" ng-click="$event.stopPropagate()">
                            <button class="button btn-sm" ng-click="edit($index)" data-toggle="modal" data-target="#editExercise">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button class="button btn-sm" ng-click="removeRoutine($index)"><i class="fa fa-remove"></i></button>
                            <button class="button btn-sm" ng-click="moveUp($index)"><i class="fa fa-chevron-up"></i></button>
                            <button class="button btn-sm" ng-click="moveUp($index, true)"><i class="fa fa-chevron-down"></i></button>
                        </span>
                    </label>
                    <article>
                        <div class="item">
                            <span for="">Sets</span>
                            <span>{{ routine.sets }}</span>
                        </div>
                        <div class="item">
                            <span for="">Set Duration</span>
                            <span>{{ routine.set_duration }} sec</span>
                        </div>
                        <div class="item">
                            <span for="">Rest</span>
                            <span>{{ routine.rest }} sec</span>
                        </div>
                        <div class="item">
                            <span for="">Cycle Rate</span>
                            <span>{{ routine.cycle_rate }} sec</span>
                        </div>
                        <div class="item">
                            <span for="">Weight</span>
                            <span>{{ routine.weight }} lbs</span>
                        </div>
                        <div class="item">
                            <span for="">Break</span>
                            <span>{{ routine.break }} sec</span>
                        </div>
                        <div class="item">
                            <span for="">Instructional</span>
                            <span><a ng-show="routine.exercise.media_link" href="javascript:void(0)" ng-click="openMedia($index)">Instructional</a></span>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <!-- Exercise Table End -->
        
        <!-- Workout Create Edit Form -->
        <div id="createworkout" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-heading">Add Workout Form</h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <div class="col-md-12">
                            <form>
                                <div class="form-group">
                                    <label for="">Workout Name</label>
                                    <input type="text" ng-model="workout.name">
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <input type="text" ng-model="workout.description">
                                </div>
                                <div class="form-group">
                                    <label for="">Workout Category</label>
                                    <select class="form-control" ng-model="workout.category" ng-options="category.name as category.name for (key, category) in categories">
                                        <option value="" disabled="true">--select--</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button ng-click="create()" ng-hide="workout_edit_mode">Save</button>
                                    <button ng-click="update()" ng-show="workout_edit_mode" data-dismiss="modal">Update</button>
                                    <button ng-click="cancel()" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End workout create form -->
        
        <!-- exercise edit form -->
        <div id="editExercise" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-heading">Edit Exercise</h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form ng-submit="save(edit_index)">
                                    <div class="form-group">
                                        <label for="">Exercise Name</label>
                                        <select ng-show="routine.edit_mode" ng-model="routine.exercise" class="form-control"
                                            ng-options="exercise as exercise.name for exercise in exercises track by exercise.id">
                                            <option value="" selected="selected">--select--</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Sets</label>
                                        <input class="form-control" type="text" ng-model="routine.sets">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Set Duration (sec)</label>
                                        <input class="form-control" type="text" ng-model="routine.set_duration">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Rest (between sets) (sec)</label>
                                        <input class="form-control" type="text" ng-model="routine.rest">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Cycle Rate (sec)</label>
                                        <input class="form-control" type="text" ng-model="routine.cycle_rate">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Weight (lbs)</label>
                                        <input class="form-control" type="text" ng-model="routine.weight">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Break (before next exercise) (sec)</label>
                                        <input class="form-control" type="text" ng-model="routine.break">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-sm">Update</button>
                                        <button type="button" class="btn btn-sm" data-dismiss="modal" ng-click="routine = {}">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End exercise edit form -->
        
        <!-- Workout Category Start -->
        <div id="workoutcategory" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Workout Categories</h1>
                                <form>
                                    <div class="form-group">
                                        <label for="">Category Name</label>
                                        <input type="text" class="form-control" ng-model="category.name" required />
                                        <small ng-show="editing_category">edit mode</small>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-sm" ng-click="createCategory()" ng-hide="editing_category">Save</button>
                                        <button class="btn btn-sm" ng-click="updateCategory()" ng-show="editing_category">Update</button>
                                        <button class="btn btn-sm" type="button" ng-click="editing_category = false; category = {}" ng-show="editing_category">Cancel</button>
                                    </div>
                                </form>
                                <br>
                                <div class="panel panel-default">
                                    <ul class="list-group">
                                        <li class="list-group-item" ng-repeat="category in categories track by $index">
                                            <span>{{ category.name }}</span>
                                            <ul class="list-inline pull-right">
                                                <li><a href="javascript:void(0)" ng-click="editCategory($index)"><i class="fa fa-pencil"></i></a></li>
                                                <li><a href="javascript:void(0)" ng-click="removeCategory($index)"><i class="fa fa-remove"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Workout Category End -->
        
        <div id="animation" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="#" data-dismiss="modal"><i class="fa fa-close"></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <iframe ng-show="selected_routine" src="{{ selected_routine.exercise.animation_link }}" frameborder="0" width="1000" height="600"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="media" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="#" data-dismiss="modal"><i class="fa fa-close"></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <iframe ng-show="selected_routine" src="{{ selected_routine.exercise.media_link }}" frameborder="0" width="1000" height="600"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- modal for begin exercise-->
        <div class="container">
          <!-- Trigger the modal with a button -->
              <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="dialog">
                    <div class="modal-content">
                        <div class="modal-header"><a href="" ng-click="closeModal()"><i class="fa fa-close"></i></a></div>
                        <div id="counter-container">
                            <div id="counter-information">
                                <div id="information-box">
                                    <iframe src="{{ timer_state.routine.exercise.animation_media }}" frameborder="0" width="200" height="200"></iframe>
                                </div>
                            </div>
                            <div id="counter-content">
                                <div id="state-label">
                                    <h3 class="{{ timer_next_state | classes }}">{{ timer_next_state | states}}</h3>
                                    <h4 style="font-size:20px;margin-top:-30px;margin-bottom:-30px;text-align:center;color:#ddd;" class="{{ timer_next_state }}">
                                        {{ timer_state.routine.exercise.name }}
                                    </h4>
                                </div>
                                <div class="timer-container">
                                    <div class="{{ timer_next_state | classes: true }}" id="seconds">00</div>
                                    <div class="{{ timer_next_state | classes: true }}" id="center">.</div>
                                    <div class="{{ timer_next_state | classes: true }}" id="milliseconds">00</div>
                                </div>
                                <div class="timer-stats">
                                    <div class="info-box">
                                        <span class="info-label">Set</span>
                                        <div class="info-content">
                                            <span class="info-count">{{ timer_state.remaining_sets | setsFilter: timer_state.routine.sets }}</span><br>
                                            <span>of {{ timer_state.routine.sets }}</span>
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="info-label">Set Duration</span>
                                        <div class="info-content">
                                            <span class="info-count">{{ timer_state.routine.set_duration }}</span><br>
                                            <span>secs</span>
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="info-label">Cycle Rate</span>
                                        <div class="info-content">
                                            <span class="info-count">{{ timer_state.routine.cycle_rate | cratefilter }}</span><br>
                                            <span>secs</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="counter-controls">
                                <button ng-click="replay_timer()" ng-disabled="disabled_replay_btn">
                                    <i class="fa fa-refresh"></i>
                                </button>
                                <button ng-click="resume_timer()" ng-show="timer_paused" ng-disabled="disabled_btnss">
                                    <i class="fa fa-play"></i>
                                </button>
                                <button ng-click="pause_timer()" ng-hide="timer_paused" ng-disabled="disabled_btnss">
                                    <i class="fa fa-pause"></i>
                                </button>
                                <button ng-click="stop_timer()" ng-disabled="disabled_stop">
                                    <i class="fa fa-stop"></i>
                                </button>
                            </div>
                        </div>
                    </div><!--.modal-content-->
                </div><!--modal-dialog-->
            </div><!--.modal fade-->
        </div><!--.container-->
    </div>
</div>
