<div ng-app="fmt-admin" ng-controller="FMTAdminPackage">
    <h2>Create Package</h2>
    <button class="button" ng-click="add = !add">Create</button>
    <hr>
    
    <div ng-show="add" ng-cloak>
        <div class="col-md-6">
            <form class="form" ng-submit="create()">
                <div class="form-group">
                    <label for="">Package Name: </label>
                    <input type="text" class="form-control" ng-model="package.name" placeholder="Package name...">
                </div>
                <div class="form-group">
                    <label for="">Package Price: </label>
                    <input type="text" class="form-control" ng-model="package.price" placeholder="Package price...">
                </div>
                <div class="form-group">
                    <label for="">Package Description: </label>
                    <textarea id="" class="form-control" name="" ng-model="package.description" cols="3" rows="3" placeholder="Package description..."></textarea>
                </div>
                <div class="form-group">
                    <label for="">Select Exercise to include: </label>
                    <div class="list-group" style="max-height:200px;overflow:auto;">
                        <button class="list-group-item" type="button" ng-repeat="exercise in exercises" ng-click="include($index)">{{ exercise.name }}</button>
                    </div>
                </div>
                <div class="form-group">
                    <button class="button btn-success">Add Package</button>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <strong>Added Exercises to Package</strong>
            <ul class="list-group">
                <li class="list-group-item" ng-repeat="exercise in added_exercises">
                    <span>{{ exercise.name }}</span>
                    <a class="pull-right" href="javascript:void(0)" ng-click="exclude($index)"><i class="fa fa-remove"></i></a>
                </li>
            </ul>
        </div>
        <br>
    </div>

    <div>
        <table class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <th scope="col" style="width: 15px; text-align: center">#</th>
                    <th scope="col">Package Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Category</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="package in packages track by $index">
                    <td>{{ package.id }}</td>
                    <td>{{ package.name }}</td>
                    <td>${{ package.price }}</td>
                    <td>Category Name</td>
                    <td>
                        <button class="button"><i class="fa fa-pencil"></i></button>
                        <button class="button" ng-click="delete($index)"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
