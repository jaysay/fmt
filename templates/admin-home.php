<div class="wrap" ng-app="fmt-admin" ng-controller="FMTExerciseCtrl" ng-cloak>
    <h1>Exercise List</h1>
    <div class="alert alert-success" ng-show="alertdisplay"><span>Successfully added new exercise!!</span></div>
    <div class="row">
        <div class="col-md-8" ng-cloak>
            <div class="table-responsive">
                <table class="table table-bordered" datatable="ng">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Exercise Category</th>
                            <th>Create Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="(key, exercise) in exercises track by $index">
                            <td>{{ exercise.name }}</td>
                            <td>{{ exercise.description | exerp }}</td>
                            <td>{{ exercise.category }}</td>
                            <td>{{ exercise.created_at | timeformat }}</td>
                            <td>
                                <button class="button" ng-click="editExercise($index)"><i class="fa fa-pencil"></i></button>
                                <button class="button" ng-click="remove($index)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-4" ng-cloak>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <a href="#collapseform" data-toggle="collapse">Create Exercise Form</a>
                </div>
                <div id="collapseform" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <form>
                            <button class="button" ng-click="submit()">
                                <span ng-hide="edit_mode">Save</span>
                            </button>
                            <button class="button" ng-click="createNew()" ng-show="edit_mode">Create New</button>
                            <button class="button" ng-click="exercise = { type: 'free' }; edit_mode = false" type="button">Clear</button>
                            <br><br>
                            <div class="form-group">
                                <label for="name">Exercise Name: </label>
                                <input class="form-control" type="text" ng-model="exercise.name" required>
                            </div>
                            <div class="form-group">
                                <label for="description">Exercise Description: </label>
                                <textarea class="form-control" cols="3" rows="0" ng-model="exercise.description" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="category">
                                    Exercise Category &nbsp;&nbsp;&nbsp;&nbsp;
                                    <ul class="list-inline pull-right">
                                        <li><a href="#collapseform" data-toggle="collapse">Add</a></li>
                                    </ul>
                                </label>
                                <select class="form-control" ng-model="exercise.category" 
                                    ng-options="category.name as category.name for (key, category) in categories">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="animation_link">Thumbnail</label><br>
                                <button type="button" ng-click="upload()">Upload</button>
                            </div>
                            <div class="form-group">
                                <label for="instructional_link">Instructional Link</label>
                                <input class="form-control" type="text" ng-model="exercise.media_link" />
                            </div>
                            <div class="form-group">
                                <label for="animation_link">Animation Link</label>
                                <input class="form-control" type="text" ng-model="exercise.animation_link" />
                            </div>
                            <div class="form-group">
                                <label for="options">Free Options</label>
                                <select class="form-control" ng-model="exercise.type" placeholder="Free Options">
                                    <option value="free" selected="selected">Free</option>
                                    <option value="paid">Paid</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- / .panel -->
            <div class="panel panel-default">
                <div class="panel-heading">Exercise Categories <span class="label label-info" ng-show="edit_mode_cat">edit mode</span></div>
                <div class="panel-body">
                    <form>
                        <div class="form-group">
                            <label for="">Category Name: </label>
                            <input class="form-control" type="text" ng-model="category.name" placeholder="category" required/>
                        </div>
                        <button type="button" class="button" ng-click="addCategory()">Create</button>
                        <button type="button" class="button" ng-click="updateCategory()" ng-show="edit_mode_cat">Update</button>
                    </form>
                    <br>
                    <input class="form-control" type="text" placeholder="search category..." ng-model="cat_search">
                    <br>
                    <div class="list-group" style="max-height: 300px; overflow: auto;">
                        <li class="list-group-item" ng-repeat="category in search_categories track by $index">
                            <strong>{{ category.name }}</strong>
                            <ul class="list-inline pull-right">
                                <li><a href="javascript:void(0)" ng-click="removeCat($index)">Remove</a></li>
                                <li><a href="javascript:void(0)" ng-click="editCat($index)">Edit</a></li>
                            </ul>
                        </li>
                    </div>
                </div>
            </div>
        </div><!-- / .col-md-4 -->
    </div><!-- / .row -->

    <!-- Modals -->
    <div id="editexercise" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-heading">
                    <h4 class="modal-header">Update Exercise</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form" ng-submit="update()">
                                <div class="form-group">
                                    <label for="">Exercise Name</label>
                                    <input class="form-control" type="text" ng-model="edit.name">
                                </div>
                                <div class="form-group">
                                    <label for="">Exercise Description</label>
                                    <textarea id="" class="form-control" ng-model="edit.description" cols="30" rows="2"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Exercise Category</label>
                                    <select class="form-control" ng-model="edit.category" 
                                        ng-options="category.name as category.name for (key, category) in categories">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Exercise Thumbnail</label>
                                    <input type="button" class="button" ng-click="upload()" value="Upload" />
                                </div>
                                <div class="form-group">
                                    <label for="">Instructional Link</label>
                                    <input class="form-control" type="" ng-model="edit.media_link">
                                </div>
                                <div class="form-group">
                                    <label for="">Animation Link</label>
                                    <input class="form-control" type="" ng-model="edit.animation_link">
                                </div>
                                <div class="form-group">
                                    <label for="">Free Option</label>
                                    <select class="form-control" ng-model="edit.type" placeholder="Free Options">
                                        <option value="free" selected="selected">Free</option>
                                        <option value="paid">Paid</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button class="button btn-md">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->
</div>

