
<body>
    <div class="temp-fix-wrap-product">
        <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <img src="<?php echo plugins_url('fmt/img/logo.png','logo.png') ?>" id="nav-logo">
            </div><!--navbar-header-->
        </div><!--container-fluid-->
    </nav><!--navbar-->
    <div class="product-wrapper">
        <div class="wrapper">
            <div class="flex-container">
                <div class="product-container">
                   <div class="img-wrapper">
                        <img src="<?php echo plugins_url('fmt/img/img1.jpg', 'img1.jpg') ?>">
                        <div class="img-wrapper-content">
                            <h1>FM - Bodyweight</h1>
                        </div>
                    </div>
                    <div class="content-details">
                        <h3>FB Burn - Smart HIIT & Strength Program to Get Fit Quick</h3>
                        <h5>$100</h5>
                        <a href="" class="btn btn-default"><i class="fa fa-shopping-cart" aria-hidden="true">Purchase</i></a>
                    </div>
                </div><!--product-container--> 
                <div class="product-container">
                    <div class="img-wrapper">
                        <img src="<?php echo plugins_url('fmt/img/img1.jpg', 'img1.jpg') ?>">
                        <div class="img-wrapper-content">
                            <h1>FM - Bodyweight</h1>
                        </div>
                    </div>
                    <div class="content-details">
                        <h3>FB Burn - Smart HIIT & Strength Program to Get Fit Quick</h3>
                        <h5>$100</h5>
                        <a href="" class="btn btn-default"><i class="fa fa-shopping-cart" aria-hidden="true">Purchase</i></a>
                    </div>
                </div><!--product-container--> 
                <div class="product-container">
                   <div class="img-wrapper">
                        <img src="<?php echo plugins_url('fmt/img/img1.jpg', 'img1.jpg') ?>">
                        <div class="img-wrapper-content">
                            <h1>FM - Bodyweight</h1>
                        </div>
                    </div>
                    <div class="content-details">
                        <h3>FB Burn - Smart HIIT & Strength Program to Get Fit Quick</h3>
                        <h5>$100</h5>
                        <a href="" class="btn btn-default" data-toggle="modal" data-target="#modalProduct"><i class="fa fa-shopping-cart" aria-hidden="true">Purchase</i></a>
                    </div>
                </div><!--product-container-->      
            </div><!--flex-container-->
        </div><!--.wrapper-->  
    </div>
        <!-- BUYOUT MODAL -->
    <div class="modal fade" id="modalProduct" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content" style="text-align: center" id="modal-content-product">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1>FM - Body Weight</h1>
                </div>
                <div class="modal-body" id="modal-body-product">
                    <div class="modal-body-content-product">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <div class="exercise-list">    
                            <h3>Exercise Included:</h3>
                            <ul>
                                <li><h4>Exercise 1</h4></li>
                                <li><h4>Exercise 2</h4></li>
                                <li><h4>Exercise 3</h4></li>
                            </ul>
                            <h1>$100</h1>
                        </div><!--exercise-list-->
                        <button class="btn btn-lg btn-default">PURCHASE</button>
                    </div><!--modal-body-content-product-->
                </div><!--modal-body-->
            </div><!--modal-content-->
        </div><!--modal-dialog-->
    </div><!--modal-->
</div>

     
</body>