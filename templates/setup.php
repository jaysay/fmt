<div class="wrap" ng-app="fmt-admin">
    <h1>FMT Settings</h1>
    <hr>
    <div class="row">
        <div ng-controller="FMTSettings"class="col-md-8">
            <div class="panel panel-default">
            <div class="panel-heading">FMT Payment Settings</div>
                <div class="panel-body">
                    <input class="form-control" type="text" placeholder="stripe key" ng-model="stripe.stripe_key"/><br>
                    <input class="form-control" type="text" placeholder="stripe secret" ng-model="stripe.stripe_secret" /><br>
                    <input class="form-control" type="" placeholder="currency" ng-model="stripe.currency" /><br>
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <button class="btn btn-info" ng-click="save()">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div ng-controller="FMTSettingsCategory" class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Workout Categories</div>
                <div class="panel-body">
                    <div>
                        <form ng-submit="createWorkout()">
                            <input type="text" placeholder="workout category name..." ng-model="workout.name" />
                            <button class="button">Save</button>
                        </form>
                    </div><br>
                    <input class="form-control" type="text" ng-model="search_cat" placeholder="search category...">
                    <br>
                    <div style="max-height:300px !important; overflow: auto;">
                        <div class="list-group">
                            <div class="list-group-item" ng-repeat="category in workout_categories track by $index">
                                <span>{{ category.name }}</span>
                                <ul class="list-inline pull-right">
                                    <li><a href="" ng-click="editWorkout($index)">Edit</a></li>
                                    <li><a href="" ng-click="removeWorkout($index)">Remove</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
