<div class="wrap">
    <div class="col-md-12">
        <div class="col-md-10">
            <h1>FM Race Pacer</h1>
            <h2>Add exercise</h2>
        </div>
        <br>
        <br>
        <div class="col-md-10">
        <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $_GET['id']; ?>">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Exercise Name:</label>
                        <input class="form-control" type="text" id="exer-name" value="<?php echo $_GET['name']; ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Categories:</label>
                        <select id="exer-category" class="form-control" name="category">
                            <?php $categories = get_categories(); ?>
                            <?php foreach ($categories as $cat) : ?>
                                <option <?php if ( $cat->name == "Uncategorized" ) { echo "selected"; } ?> value="<?php echo $cat->cat_ID; ?>">
                                    <?php echo $cat->name; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Upload Video: </label><br>
                        <button id="upload-avatar" class="button" type="button">Upload</button>
                        <div id="exer-avatar"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea  id="exer-description" class="form-control" name="" cols="10" rows="5"><?php echo $_GET['desc']; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php if($_GET['edit']) : ?>
                    <button class="button button-primary" id="update-exercise-btn" data-redirect='<?php echo admin_url("admin.php?page=fmt_admin_page_slug"); ?>'>Update</button>
                <?php else : ?>
                <button id="create-exercise-button" class="button button-primary" data-redirect='<?php echo admin_url("admin.php?page=fmt_admin_page_slug"); ?>'>Create Exercise</button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
