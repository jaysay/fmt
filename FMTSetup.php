<?php

namespace FMT;

if ( !class_exists("FMTSetup") ) {

    class FMTSetup {

        private $dir_name = "fmt";

        public function setup()
        {
            add_action('admin_enqueue_scripts', [&$this, 'enqueue']);
            add_action('wp_enqueue_scripts',    [&$this, 'enqueue']);
            add_action('admin_menu',            [&$this, 'addAdminMenu']);
            add_shortcode("fm_timezone",        [&$this, "displayWorkoutTemplate"]);
            add_shortcode("fm_products",        [&$this, "displayProducts"]);
            add_shortcode("fm_package_checkout",        [&$this, "displayPackageCheckout"]);
        }

        public function enqueue()
        {
            wp_enqueue_media();
            wp_enqueue_style("bootstrap", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", [], "3.3.7", "all");
            wp_enqueue_script("bootstrapjs", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", [], "3.3.7", "all");
            wp_enqueue_script("angularjs", "https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js", [], "1.6.5", "all");
            wp_enqueue_style("font_awesome", "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css", [], "4.7.0", "all");
            wp_enqueue_style("font-karla", "https://fonts.googleapis.com/css?family=Karla:400,400i,700,700i", [], "4.7.0", "all");
            wp_enqueue_style("angulardatatables", "https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.2/css/angular-datatables.min.css", [], "0.6.2", "all");
            wp_enqueue_style("angulardatatablesbootstrap", "https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.2/plugins/bootstrap/angular-datatables.bootstrap.js", [], "0.6.2", "all");
            wp_enqueue_style("angulardatatablescss", "https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css", [], "1.10.16", "all");
            wp_enqueue_style("fmt_main_css", plugins_url( $this->dir_name . "/css/main.css"), [], "1.0.0", "all");
            wp_enqueue_script("jquery", "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js", [], "3.2.1", true);
            wp_enqueue_script("datatables-js", "https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js", [], "1.10.16", true);
            wp_enqueue_script("angular-datatables-js", "https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.2/angular-datatables.min.js", [], "0.6.2", true);
            wp_enqueue_script("fmt_main_js", plugins_url( $this->dir_name . "/js/main.js"), [], "1.0.0", true);
            wp_enqueue_script("fmt_angular_js", plugins_url( $this->dir_name . "/js/fmt.js"), [], "1.0.0", true);
            wp_enqueue_script("controls_angular_js", plugins_url( $this->dir_name . "/js/controls.js"), [], "1.0.0", true);
            wp_enqueue_script("fmt_admin_js", plugins_url( $this->dir_name . "/js/admin.js"), [], "1.0.0", true);
            wp_enqueue_script("bootstrapjsdatatables", "https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.2/plugins/bootstrap/angular-datatables.bootstrap.min.js", [], "0.6.2", "all");
            wp_localize_script('fmt_main_js', 'localObj', ['ajaxurl' => admin_url('admin-ajax.php')]);
        }

        public function addAdminMenu()
        {
            add_menu_page(
                'FMT',
                'FMT Race Pacer', 
                'manage_options',
                "fmt_admin_page_slug",
                [&$this, "displayAdminHome"],
                'dashicons-universal-access',
                6
            );

            /*
            add_submenu_page(
                "fmt_admin_page_slug",
                "Package Page",
                "Package",
                'manage_options',
                "fmt_packages_slug",
                [&$this, "displayPackages"]
            );

            add_submenu_page(
                "fmt_admin_page_slug",
                "Add Exercise Page",
                "Add Exercise",
                'manage_options',
                "fmt_add_exercise_slug",
                [&$this, "displayAddExercise"]
            );

            add_submenu_page(
                "fmt_admin_page_slug",
                "Create Package Page",
                "Create Package",
                'manage_options',
                "fmt_create_package_slug",
                [&$this, "displayCreatePackage"]
            );

            add_submenu_page(
                "fmt_admin_page_slug",
                "Settings",
                "Settings",
                'manage_options',
                "fmt_setup_slug",
                [&$this, "displaySetup"]
            );
             */
        }

        public function displayAddExercise()
        {
            include (plugin_dir_path(__FILE__) . "templates/add-exercise.php");
        }
        public function displayProducts()
        {
            $atts = shortcode_atts(
                [ "title" => "FM Race Pacer - Training Tool" ],
                $atts
            );

            ob_start();
            include(plugin_dir_path(__FILE__) . "templates/products.php");
            return ob_get_clean();
        }
        public function displayPackageCheckout()
        {
            $atts = shortcode_atts(
                [ "title" => "FM Race Pacer - Training Tool" ],
                $atts
            );

            ob_start();
            include(plugin_dir_path(__FILE__) . "templates/package-checkout.php");
            return ob_get_clean();
        }
        public function displayAdminHome() 
        {
            include (plugin_dir_path(__FILE__) . "templates/admin-home.php");
        }

        public function displayCreatePackage()
        {
            include (plugin_dir_path(__FILE__) . "templates/create-package.php");
        }

        public function displayPackages()
        {
            include (plugin_dir_path(__FILE__) . "templates/packages.php");
        }

        public function displayWorkoutTemplate()
        {
            $atts = shortcode_atts(
                [ "title" => "FM Race Pacer - Training Tool" ],
                $atts
            );

            ob_start();
            include(plugin_dir_path(__FILE__) . "templates/workout-template.php");
            return ob_get_clean();
        }

        public function displaySetup()
        {
            include (plugin_dir_path(__FILE__) . "templates/setup.php");
        }
    }
}

?>
