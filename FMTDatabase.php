<?php

namespace FMT;

class FMTDatabase {

    private $workouts_table         = null;
    private $categories_table       = null;
    private $wpdb                   = null;
    private $exercise_table         = null;
    private $workout_exercise_table = null;

    public function __construct()
    {
        global $wpdb;
        $this->wpdb                     = $wpdb;

        // Fixed
        $this->settings_table         = $this->wpdb->prefix . "fmt_settings";
        $this->category_table         = $this->wpdb->prefix . "fmt_categories";
        $this->workout_category_table = $this->wpdb->prefix . "fmt_workout_categories";
        $this->workout_table          = $this->wpdb->prefix . "fmt_workout";
        $this->exercise_table         = $this->wpdb->prefix . "fmt_exercises";
        $this->routine_table          = $this->wpdb->prefix . "fmt_routine";
        $this->alerts_table           = $this->wpdb->prefix . "fmt_alerts";
        $this->package_table          = $this->wpdb->prefix . "fmt_packages";
        $this->package_exercise_table = $this->wpdb->prefix . "fmt_package_exercise";
        $this->package_user_table     = $this->wpdb->prefix . "fmt_package_user";
    }

    public function setupDatabase()
    {
        $charset = $this->wpdb->get_charset_collate();

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $this->createWorkoutTable( $charset );
        $this->createExerciseTable( $charset );
        $this->createExerciseRoutine( $charset );
        $this->createPackages( $charset );
        $this->createPackagesExercise( $charset );
        $this->createAlert( $charset );
        $this->createCategory( $charset );
        $this->createWorkoutCategory( $charset );
        $this->createPackageUser( $charset );
        $this->createSettings( $charset );
    }

    private function createWorkoutTable( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->workout_table (".
            "id INTEGER(10) UNSIGNED AUTO_INCREMENT,".
            "name VARCHAR(255),".
            "description TEXT,".
            "category VARCHAR(255),".
            "user_id INTEGER(10),".
            "created_at TIMESTAMP,".
            "PRIMARY KEY(id)".
            ") $charset;"; 
        dbDelta($sql);
    }

    private function createExerciseTable( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->exercise_table (".
            "id INTEGER(10) UNSIGNED AUTO_INCREMENT,".
            "name VARCHAR(255),".
            "description TEXT,".
            "type VARCHAR(255),".
            "category VARCHAR(255),".
            "media_link VARCHAR(255),".
            "animation_link VARCHAR(255),".
            "thumbnail VARCHAR(255),".
            "created_at TIMESTAMP,".
            "PRIMARY KEY(id)".
            ") $charset;"; 
        dbDelta($sql);
    }

    private function createExerciseRoutine( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->routine_table (".
            "id INTEGER(10) UNSIGNED AUTO_INCREMENT,".
            "cycle_rate FLOAT(10),".
            "set_duration FLOAT(10),".
            "rest FLOAT(10),".
            "sets FLOAT(10),".
            "weight FLOAT(10),".
            "break FLOAT(10),".
            "created_at TIMESTAMP,".
            "exercise_id INTEGER(10) UNSIGNED,".
            "workout_id INTEGER(10) UNSIGNED NULL,".  "PRIMARY KEY(id),".
            "FOREIGN KEY(workout_id) REFERENCES $this->workout_table(id)".
            ") $charset;"; 
        dbDelta($sql);
    }

    private function createPackages( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->package_table (".
            "id INTEGER(10) UNSIGNED AUTO_INCREMENT,".
            "name VARCHAR(255),".
            "price INTEGER(10),".
            "description TEXT,".
            "category VARCHAR(255),".
            "created_at TIMESTAMP,".
            "PRIMARY KEY(id)".
            ") $charset;";
        dbDelta( $sql );
    }

    private function createPackagesExercise( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->package_exercise_table (".
            "package_id INTEGER(10) UNSIGNED,".
            "exercise_id INTEGER(10) UNSIGNED,".
            "created_at TIMESTAMP,".
            "PRIMARY KEY(package_id, exercise_id),".
            "FOREIGN KEY(package_id) REFERENCES $this->package_table(id),".
            "FOREIGN KEY(exercise_id) REFERENCES $this->exercise_table(id)".
            ") $charset;";
        dbDelta( $sql );
    }

    private function createPackageUser( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->package_user_table (".
            "package_id INTEGER(10) UNSIGNED,".
            "user_id INTEGER(10),".
            "created_at TIMESTAMP,".
            "PRIMARY KEY(package_id),".
            "FOREIGN KEY(package_id) REFERENCES $this->package_table(id)".
            ") $charset;";
        dbDelta( $sql );
    }

    private function createAlert( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->alerts_table (".
            "id INTEGER(10) UNSIGNED AUTO_INCREMENT,".
            "name INTEGER(10) UNSIGNED,".
            "audio VARCHAR(255),".
            "PRIMARY KEY(id)".
            ") $charset;";
        dbDelta( $sql );
    }

    private function createCategory( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->category_table (".
            "id INTEGER(10) UNSIGNED AUTO_INCREMENT,".
            "name VARCHAR(255),".
            "created_at TIMESTAMP,".
            "PRIMARY KEY(id)".
            ") $charset;";
        dbDelta( $sql );
    }

    private function createWorkoutCategory( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->workout_category_table (".
            "id INTEGER(10) UNSIGNED AUTO_INCREMENT,".
            "name VARCHAR(255),".
            "created_at TIMESTAMP,".
            "PRIMARY KEY(id)".
            ") $charset;";
        dbDelta( $sql );
    }

    private function createSettings( $charset )
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->settings_table (".
            "id INTEGER(10) UNSIGNED,".
            "stripe_key VARCHAR(255),".
            "stripe_secret VARCHAR(255),".
            "currency VARCHAR(255),".
            "PRIMARY KEY(id)".
            ") $charset;";
        dbDelta( $sql );
    }
}

?>
