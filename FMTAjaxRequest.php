<?php

namespace FMT;

require_once("models/Package.php");
require_once("models/Alert.php");
require_once("models/Audio.php");
require_once("models/Exercises.php");
require_once("models/Routine.php");
require_once("models/Workout.php");
require_once("models/WorkoutCategory.php");
require_once("models/Category.php");
require_once("models/Settings.php");
require_once("models/PackageExercise.php");

use Alert;
use Audio;
use Package;
use PackageExercise;
use Exercises;
use Routine;
use Workout;
use Category;
use WorkoutCategory;
use Settings;

class FMTAjaxRequest {

    public function setupRoutes()
    {
        // Package Routes
        add_action("wp_ajax_create_package",                  [&$this, "createPackage"]);
        add_action("wp_ajax_update_package",                  [&$this, "updatePackage"]);
        add_action("wp_ajax_delete_package",                  [&$this, "deletePackage"]);
        add_action("wp_ajax_get_packages",                    [&$this, "getPackages"]);

        // Audios Routes
        add_action("wp_ajax_create_audio",              [&$this, "createAudio"]);
        add_action("wp_ajax_get_audios",                [&$this, "getAudios"]);

        add_action("wp_ajax_delete_workout",            [&$this, "deleteWorkout"]);

        //add_action("wp_ajax_update_workout",            [&$this, "updateWorkout"]);
        //add_action("wp_ajax_remove_workout",            [&$this, "removeWorkout"]);
        add_action("wp_ajax_add_workout",               [&$this, "addWorkout"]);
        add_action("wp_ajax_get_workouts",              [&$this, "getWorkouts"]);
        add_action("wp_ajax_add_exercise",              [&$this, "addExerciseList"]);
        add_action("wp_ajax_add_exercise_user",         [&$this, "addExerciseUser"]);
        add_action("wp_ajax_fetch_exercise_list",       [&$this, "fetchExerciseList"]);
        add_action("wp_ajax_change_selected_workout",   [&$this, "changeSelectedWorkout"]);
        add_action("wp_ajax_fetch_workout_exercises",   [&$this, "fetchWorkoutExercises"]);
        add_action("wp_ajax_update_workout_exercises_input",          [&$this, "updateWorkoutExerciseInput"]);
        add_action("wp_ajax_update_workout_exercises_selected_exer",  [&$this, "updateWorkoutExercisesSelectedExer"]);
        add_action("wp_ajax_update_workout_exercises_is_checked",     [&$this, "updateWorkoutExercisesIsChecked"]);

        add_action("wp_ajax_get_exercise", [&$this, "getExercise"]);
        add_action("wp_ajax_create_exercise", [&$this, "createExercise"]);
        add_action("wp_ajax_remove_exercise", [&$this, "removeExercise"]);
        add_action("wp_ajax_update_exercise", [&$this, "updateExercise"]);

        add_action("wp_ajax_get_workout", [&$this, "getWorkout"]);
        add_action("wp_ajax_update_workout", [&$this, "updateWorkout"]);
        add_action("wp_ajax_get_workout_routine", [&$this, "getWorkoutRoutine"]);
        add_action("wp_ajax_create_workout", [&$this, "createWorkout"]);
        add_action("wp_ajax_remove_workout", [&$this, "removeWorkout"]);

        add_action("wp_ajax_create_routine", [&$this, "createRoutine"]);
        add_action("wp_ajax_remove_routine", [&$this, "removeRoutine"]);
        add_action("wp_ajax_update_routine", [&$this, "updateRoutine"]);

        // Categories
        add_action("wp_ajax_get_categories", [&$this, "getCategory"]);
        add_action("wp_ajax_create_category", [&$this, "createCategory"]);
        add_action("wp_ajax_remove_category", [&$this, "removeCategory"]);
        add_action("wp_ajax_update_category", [&$this, "updateCategory"]);

        // Workout Categories
        add_action("wp_ajax_get_workout_categories", [&$this, "getWorkoutCategory"]);
        add_action("wp_ajax_create_workout_category", [&$this, "createWorkoutCategory"]);
        add_action("wp_ajax_update_workout_category", [&$this, "updateWorkoutCategory"]);
        add_action("wp_ajax_remove_workout_category", [&$this, "removeWorkoutCategory"]);

        // Settings
        add_action("wp_ajax_get_settings", [&$this, "getSettings"]);
        add_action("wp_ajax_create_settings", [&$this, "createSettings"]);
        add_action("wp_ajax_update_settings", [&$this, "updateSettings"]);

        // Packages
        add_action("wp_ajax_create_package", [&$this, "createPackage"]);
        add_action("wp_ajax_get_packages", [&$this, "getPackages"]);
        add_action("wp_ajax_remove_package", [&$this, "removePackage"]);
        return $this;
    }

    public function removePackage()
    {
        $client = $this->getVars()->wpdb;
        $package_table = $client->prefix . 'fmt_packages';
        $pexercise_table = $client->prefix . 'fmt_package_exercise';
        $package = new Package($package_table, $client);
        $exercise = new PackageExercise($pexercise_table, $client);

        $package_id = $_POST['package_id'];

        $res = $exercise->cdelete([ "package_id" => $package_id ]);
        $result = $package->delete($package_id);

        wp_die();
    }

    public function getPackages()
    {
        $client = $this->getVars()->wpdb;
        $package_table = $client->prefix . 'fmt_packages';
        $package = new Package($package_table, $client);

        $result = $package->all();

        wp_die(json_encode($result));
    }

    public function createPackage()
    {
        $client = $this->getVars()->wpdb;
        $package_table = $client->prefix . 'fmt_packages';
        $pexercise_table = $client->prefix . 'fmt_package_exercise';
        $package = new Package($package_table, $client);
        $package_exercise = new PackageExercise($pexercise_table, $client);

        $data = $_POST['package'];
        $exercises = $_POST['exercises'];

        $result = $package->create($data);
        $package_id = $result[0]->id;

        foreach ($exercises as $exercise) {
            $arr = ['package_id' => $package_id, 'exercise_id' => $exercise['id'] ];
            $package_exercise->create($arr);
        }

        wp_die(json_encode($result));
    }
    
    public function updateSettings()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_settings';
        $setting = new Settings($table, $client);
        $settings = $_POST['settings'];
        $setting_id = $_POST['setting_id'];

        $result = $setting->update($setting_id, $settings);

        wp_die(json_encode($result));
    }

    public function getSettings()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_settings';
        $category = new Settings($table, $client);
        $result = $category->all();
        wp_die(json_encode($result));
    }

    public function createSettings()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_settings';
        $setting = new Settings($table, $client);
        $settings = $_POST['settings'];

        $result = $setting->create($settings);

        wp_die(json_encode($result));
    }

    public function getWorkoutCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout_categories';
        $category = new WorkoutCategory($table, $client);
        $result = $category->all();
        wp_die(json_encode($result));
    }

    public function createWorkoutCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout_categories';
        $category = new WorkoutCategory($table, $client);
        $result = $category->create($_POST['category']);
        wp_die(json_encode($result));
    }

    public function removeWorkoutCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout_categories';
        $category = new WorkoutCategory($table, $client);
        $category_id = $_POST['category_id'];
        $result = $category->delete($category_id);
        wp_die(json_encode($result));
    }

    public function updateWorkoutCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout_categories';
        $category = new WorkoutCategory($table, $client);
        $category_id = $_POST['category_id'];
        $category_data = $_POST['category'];

        $result = $category->update($category_id, $category_data);

        wp_die(json_encode($result));
    }

    public function getCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_categories';
        $category = new Category($table, $client);
        $result = $category->all();
        wp_die(json_encode($result));
    }

    public function createCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_categories';
        $category = new Category($table, $client);
        $result = $category->create($_POST['category']);
        wp_die(json_encode($result));
    }

    public function removeCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_categories';
        $category = new Category($table, $client);
        $category_id = $_POST['category_id'];
        $result = $category->delete($category_id);
        wp_die(json_encode($result));
    }

    public function updateCategory()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_categories';
        $category = new Category($table, $client);
        $category_id = $_POST['category_id'];
        $category_data = $_POST['category'];

        $result = $category->update($category_id, $category_data);

        wp_die(json_encode($result));
    }

    public function createRoutine()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_routine';
        $routine = new Routine($table, $client);
        $data = $_POST['routine'];

        $result = $routine->create($data);

        wp_die(json_encode($result));
    }

    public function removeRoutine()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_routine';
        $routine = new Routine($table, $client);
        $routine_id = $_POST['routine_id'];

        $routine->delete($routine_id);
    }

    public function updateRoutine()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_routine';
        $routine = new Routine($table, $client);
        $routine_id = $_POST['routine_id'];
        $update_routine = $_POST['routine'];

        $result = $routine->update($routine_id, $update_routine);

        wp_die(json_encode($result));
    }

    public function getWorkoutRoutine()
    {
        $var = $this->getVars();
        $client = $this->getVars()->wpdb;
        $routine_table = $client->prefix . 'fmt_routine';
        $workout_table = $client->prefix . 'fmt_workout';
        $exercise_table = $client->prefix . 'fmt_exercises';
        $workout_id = $_GET['workout_id'];

        $query = "SELECT * FROM $routine_table as routine ";
        $query .= "WHERE routine.workout_id = $workout_id";
        $result = $var->wpdb->get_results($query);

        $results = array_map(function($routine) use ($exercise_table, $var) {
            $query = "SELECT * FROM $exercise_table WHERE id = " . $routine->exercise_id;
            $result = $var->wpdb->get_results($query);
            $routine->exercise = $result[0];
            return $routine;
        }, $result);

        wp_die(json_encode($results));
    }

    public function getWorkout()
    {
        $var = $this->getVars();
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout';
        $workout = new Workout($table, $client);

        wp_die(json_encode($workout->all($var->user->ID)));
    }

    public function createWorkout()
    {
        $var = $this->getVars();
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout';
        $workout = new Workout($table, $client);
        $data = $_POST['workout'];
        $data['user_id'] = $var->user->ID;
            
        $result = $workout->create($data);

        wp_die(json_encode($result));
    }

    public function removeWorkout()
    {
        $var = $this->getVars();
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout';
        $workout = new Workout($table, $client);
        $id = $_POST['workout_id'];

        $query = "DELETE FROM wp_fmt_routine  WHERE workout_id = " . $id;

        $var->wpdb->get_results($query);

        $result = $workout->delete($id);

        wp_die($result);
    }

    public function updateExercise()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_exercises';
        $exercise = new Exercises($table, $client);
        $data = $_POST['exercise'];
        $exercise_id = $_POST['exercise_id'];

        $result = $exercise->update($exercise_id, $data);

        wp_die(json_encode($result));
    }

    public function createExercise()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_exercises';
        $exercise = new Exercises($table, $client);
        $data = $_POST['exercise'];

        $result = $exercise->create($data);

        wp_die(json_encode($result));
    }

    public function getExercise()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_exercises';
        $exercise = new Exercises($table, $client);

        $result = $exercise->all();

        wp_die(json_encode($result));
    }

    public function removeExercise()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_exercises';
        $exercise = new Exercises($table, $client);
        $exercises_id = $_POST['exercises'];
        $result = [];

        foreach ($exercises_id as $id) {
            $result[] = $exercise->delete($id);
        }
        wp_die(json_encode($result));
    }

    public function deleteWorkout()
    {
        $var = $this->getVars();
        $id = $_POST['data']['workout_id'];

        $query = "DELETE FROM " . $var->workouts_table . " WHERE workout_id='$id'";
        $var->wpdb->get_results($query);

        wp_die($id);
    }

    public function localizeObject()
    {
        wp_localize_script('fmt_main_js', 'localObj', ['ajaxurl' => admin_url('admin-ajax.php')]);

        return $this;
    }

    /**
     * Create Audio
     */
    public function createAudio()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_audio';
        $audio = new Audio($table, $client);

        $data = $_POST['data'];
        $result = $audio->create($data);
        wp_die(json_encode($result));
    }

    /**
     * Create Audio
     */
    public function getAudios()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_audio';
        $audio = new Audio($table, $client);

        wp_die(json_encode($audio->all()));
    }

    /**
     * Package Create
     */
    public function createPackages()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_package_list';
        $package = new Package($table, $client);

        $data = $_POST['data']['package'];
        $result = $package->create($data);
        wp_die(json_encode($result));
    }

    /**
     * Update Package
     */
    public function updatePackage()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_package_list';
        $package = new Package($table, $client);

        $data = $_POST['data']['package'];
        $id = $_POST['data']['package_id'];
        $result = $package->update($id, $data);
        wp_die(json_encode($result));
    }

    /**
     * Delete Package
     */
    public function deletePackage()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_package_list';
        $package = new Package($table, $client);

        $list = $_POST['data'];
        foreach($list as $id) {
            $result = $package->delete($id);
        }
        wp_die(json_encode($result));
    }

    public function getPackagess()
    {
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_package_list';
        $package = new Package($table, $client);

        wp_die(json_encode($package->all()));
    }
    
    public function fetchExerciseList()
    {
        $var = $this->getVars();

        $exers = $var->wpdb->get_results("SELECT * FROM " . $var->exercise_list_table );

        foreach( $exers as $key => $exer ) {
            $exers[ $key ]->category_name = get_the_category_by_ID( $exer->category_id );
        }

        wp_die( json_encode($exers) );
    }

    public function removesWorkout() 
    {
        $var = $this->getVars();

        $ids = $_POST['data']['ids'];

        $in_ids = implode(",", $ids);

        $query = "DELETE FROM " . $var->exercise_list_table . " WHERE exercise_id IN (" . $in_ids . ")";

        $exer = $var->wpdb->get_results($query);
    }

    public function updateWorkout()
    {
        $var = $this->getVars();
        $client = $this->getVars()->wpdb;
        $table = $client->prefix . 'fmt_workout';
        $workout = new Workout($table, $client);
        $updated_workout = $_POST['workout'];
        $workout_id = $_POST['id'];

        $result = $workout->update($workout_id, $updated_workout);

        wp_die(json_encode($result));
    }

    public function addExerciseUser()
    {
        $var = $this->getVars();
        $var->wpdb->insert( $var->exercise_table, [
            "cycle_rate"    => $_POST['data']['cycle_rate'],
            "set_duration"  => $_POST['data']['set_duration'],
            "rest"          => $_POST['data']['rest'],
            "sets"          => $_POST['data']['sets'],
            "user_weight"   => $_POST['data']['user_weight'],
            "break"         => $_POST['data']['break'],
        ]);

        $this->setWorkoutExercise([
            "workout_id"    => $_POST['data']['workout_id'],
            "exercise_id"   => $var->wpdb->insert_id,
            "list_index"    => $_POST['data']['list_index'],
            "is_checked"    => 1,
        ]);

        wp_die(1);
    }

    public function addWorkout()
    {
        $this->unselectWorkout();

        $var = $this->getVars();

        $var->wpdb->insert( $var->workouts_table, [
            "workout_name"  => $_POST["data"]["workout_name"],
            "category_id"   => $_POST["data"]["category_id"],
            "description"   => $_POST["data"]["description"],
            "is_selected"   => 1,
            "user_id"       => $var->user->ID,
        ]);

        wp_die(json_encode($_POST));
    }

    public function updateWorkoutExerciseInput()
    {
        $var = $this->getVars();

        if ( isset($_POST['data']['cycle_rate']) ) {
            $var->wpdb->update( $var->exercise_table, ["cycle_rate" => $_POST['data']['cycle_rate']], [
                "exercise_id" => $_POST['data']['exer_id']
            ]);
        }
        if ( isset($_POST['data']['set_duration']) ) {
            $var->wpdb->update( $var->exercise_table, ["set_duration" => $_POST['data']['set_duration']], [
                "exercise_id" => $_POST['data']['exer_id']
            ]);
        }
        if ( isset($_POST['data']['rest']) ) {
            $var->wpdb->update( $var->exercise_table, ["rest" => $_POST['data']['rest']], [
                "exercise_id" => $_POST['data']['exer_id']
            ]);
        }
        if ( isset($_POST['data']['sets']) ) {
            $var->wpdb->update( $var->exercise_table, ["sets" => $_POST['data']['sets']], [
                "exercise_id" => $_POST['data']['exer_id']
            ]);
        }
        if ( isset($_POST['data']['user_weight']) ) {
            $var->wpdb->update( $var->exercise_table, ["user_weight" => $_POST['data']['user_weight']], [
                "exercise_id" => $_POST['data']['exer_id']
            ]);
        }
        if ( isset($_POST['data']['break']) ) {
            $var->wpdb->update( $var->exercise_table, ["break" => $_POST['data']['break']], [
                "exercise_id" => $_POST['data']['exer_id']
            ]);
        }
    }

    public function updateWorkoutExercisesIsChecked()
    {
        $var = $this->getVars();

        $var->wpdb->update( $var->workout_exercise_table, ["is_checked" => $_POST['data']['is_checked']], [
            "workout_exercise_id" => $_POST['data']['workout_exercise_id']
        ]);
    }

    public function updateWorkoutExercisesSelectedExer()
    {
        $var = $this->getVars();

        $var->wpdb->update( $var->workout_exercise_table, ["selected_exer_list" => $_POST['data']['selected_exer_list']], [
            "workout_exercise_id" => $_POST['data']['workout_exercise_id']
        ]);
    }

    public function fetchWorkoutExercises()
    {
        $var = $this->getVars();

        $workout_exers = $var->wpdb->get_results("SELECT * FROM " . $var->workout_exercise_table . " WHERE workout_id='" . $_POST['data']['workout_id'] . "'");

        $exers = [];
        foreach ($workout_exers as $workout_exer) {
            array_push( $exers, [
                'workout_exers'     => $workout_exer,
                'exers'             => $var->wpdb->get_results("SELECT * FROM " . $var->exercise_table . " WHERE exercise_id='".$workout_exer->exercise_id."'" )[0]
            ]);
        }

        wp_die(json_encode( $exers ));
    }

    public function getWorkouts()
    {
        $var = $this->getVars();

        $workouts = $var->wpdb->get_results("SELECT * FROM " . $var->workouts_table . " WHERE user_id='" . $var->user->ID . "'");
        wp_die(json_encode( $workouts ));
    }

    public function changeSelectedWorkout()
    {
        $this->unselectWorkout();
        $selected = $this->selectWorkout( $_POST['data']['workout_id'] );
        wp_die( $selected->workout_id );
    }

    private function selectWorkout( $workout_id )
    {
        $var = $this->getVars();

        $var->wpdb->update( $var->workouts_table, ["is_selected" => 1], [
            "user_id"       => $var->user->ID, 
            "workout_id"    => $workout_id
        ]);

        return (object) [
            'workout_id'     => $workout_id
        ];
    }

    private function unselectWorkout()
    {
        $var = $this->getVars();

        $var->wpdb->update( $var->workouts_table, ["is_selected" => 0], ["user_id" => $var->user->ID]);
    }

    public function addExerciseList()
    {
        $var = $this->getVars();

        $var->wpdb->insert( $var->exercise_list_table, [
            "exercise_name" => $_POST['data']['exercise_name'],
            "description"   => $_POST['data']['description'],
            "category_id"   => $_POST['data']['category_id'],
            "image"         => $_POST['data']['avatar'],
        ]);

        return (object) ["insert_id" => $var->wpdb->insert_id ];
    }

    public function setWorkoutExercise( $data )
    {
        $var = $this->getVars();

        $var->wpdb->insert( $var->workout_exercise_table, [
            "workout_id"    => $data['workout_id'],
            "exercise_id"   => $data['exercise_id'],
            "list_index"    => $data['list_index'],
            "is_checked"    => $data['is_checked'],
        ]);

        return (object) ["insert_id" => $var->wpdb->insert_id ];
    }

    private function getVars()
    {
        global $wpdb;
        global $current_user;
        return (object) [
            "wpdb"                      => $wpdb,
            "user"                      => $current_user,
            "workouts_table"            => $wpdb->prefix . "fmt_workout",
            "exercise_table"            => $wpdb->prefix . "fmt_exercise",
            "exercise_list_table"       => $wpdb->prefix . "fmt_exercise_list",
            "workout_exercise_table"    => $wpdb->prefix . "fmt_workout_exercise",
        ];
    }
}

?>
