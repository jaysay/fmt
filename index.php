<?php
/*
 * Plugin name: FM Race Pacer
 * Description: FM Race Pacer workout plugin
 * version: 1.1
 */

require_once ("FMTSetup.php");
require_once ("FMTAjaxRequest.php"); require_once ("FMTDatabase.php");

use FMT\FMTSetup;
use FMT\FMTAjaxRequest;
use FMT\FMTDatabase;

if ( !class_exists( "FMTimezone" ) ) {
    class FMTimezone {

        public function FMTimezone()
        {
            $this->callFMTSetup();
            $this->callFMTAjaxRequest();
            $this->callFMTDatabase();
        }

        private function callFMTSetup()
        {
            (new FMTSetup())->setup();
        }

        private function callFMTAjaxRequest()
        {
            (new FMTAjaxRequest())
                ->setupRoutes()
                ->localizeObject();
        }

        private function callFMTDatabase()
        {
            (new FMTDatabase())->setupDatabase();
        }

        private function getUserId()
        {
            global $current_user;
            return $current_user->ID;
        }
    }

    new FMTimezone();
}

function plugin_deactiviation_hook() {
    global $wpdb;
    $tables = [
        $wpdb->prefix . "fmt_settings",
        $wpdb->prefix . "fmt_alerts",
        $wpdb->prefix . "fmt_workout_categories",
        $wpdb->prefix . "fmt_categories",
        $wpdb->prefix . "fmt_package_user",
        $wpdb->prefix . "fmt_routine",
        $wpdb->prefix . "fmt_workout",
        $wpdb->prefix . "fmt_package_exercise",
        $wpdb->prefix . "fmt_exercises",
        $wpdb->prefix . "fmt_packages",
    ];

    $sql = "DROP TABLE IF EXISTS ";
    $wpdb->query($sql . $tables[0]);
    $wpdb->query($sql . $tables[1]);
    $wpdb->query($sql . $tables[2]);
    $wpdb->query($sql . $tables[3]);
    $wpdb->query($sql . $tables[4]);
    $wpdb->query($sql . $tables[5]);
    $wpdb->query($sql . $tables[6]);
    $wpdb->query($sql . $tables[7]);
    $wpdb->query($sql . $tables[8]);
    $wpdb->query($sql . $tables[9]);
}

register_deactivation_hook(__FILE__, 'plugin_deactiviation_hook');

?>
