angular.module('fmt-admin', ['datatables'])
.filter('exerp', function() {
  return function(data) {
    return data
  }
})
.filter('timeformat', function() {
  return function(input) {
    let d = new Date(input);
    return (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
  }
})
.constant('fmturl', localObj.ajaxurl)
.factory('Http', function(fmturl) {
  let http = this;
  
  http.post = function(data, callback) {
    $.ajax({
      url: fmturl,
      type: 'post',
      data: data,
      success: callback,
    })
  }

  http.get = function(data, callback) {
    $.ajax({
      url: fmturl,
      type: 'get',
      data: data,
      success: callback,
    })
  }
  
  return http;
})
.controller('FMTSettings', [ "$scope", "Http", function($scope, Http) {
  
  $scope.settings = null;

  function init() {
    Http.get({ action: 'get_settings' }, function(response) {
      $scope.settings = JSON.parse(response)[0];
      $scope.stripe = JSON.parse(response)[0];
      $scope.$apply();
    });
  }

  $scope.save = function() {
    if ($scope.settings) {
      update();
    } else { create();
    }
  }

  function create() {
    Http.post({ action: 'create_settings', settings: $scope.stripe });
  }

  function update() {
    Http.post({ action: 'update_settings', setting_id: $scope.settings.id, settings: $scope.stripe })
  }

  init();

}])
.controller('FMTSettingsCategory', [ "$scope", "Http", "$filter", function($scope, Http, $filter) {

  $scope.categories = [];
  $scope.category = {};

  $scope.$watch('search_cat', function(newval) {
    let categories = angular.copy($scope.categories);
    if (newval) {
      let result = categories.filter(function(category) {
        return category.name.toLowerCase().indexOf(newval.toLowerCase()) >= 0;
      });
      $scope.workout_categories = $filter('orderBy')(result, ['name', '-created_at']);
    } else {
      $scope.workout_categories = $filter('orderBy')(categories, ['name', '-created_at']);
    }
  });

  function init() {
    Http.get({ action: 'get_workout_categories' }, function(response) {
      $scope.workout_categories = $filter('orderBy')(JSON.parse(response), ['name', '-created_at']);
      $scope.categories = $filter('orderBy')(JSON.parse(response), ['name', '-created_at']);
      $scope.$apply();
    });
  }

  $scope.createWorkout = function() {
    if ($scope.edit_mode) {
      let category = $scope.workout;
      Http.post({ action: 'update_workout_category', category: category, category_id: category.id }, function(response) {
        $scope.workout_categories[$scope.workout_index] = category;
        $scope.workout = {}
        $scope.$apply();
        alert('Workout category has been updated!');
      });
    } else {
      Http.post({ action: 'create_workout_category', category: $scope.workout }, function(response) {
        $scope.workout_categories.push(JSON.parse(response)[0]);
        $scope.categories.push(JSON.parse(response)[0]);
        $scope.workout = {}
        $scope.$apply();
      });
    }
  }

  $scope.removeWorkout = function(index) {
    let category_id = $scope.workout_categories[index].id;
    let remove = confirm('Are you sure you want to remove this workout category ' + $scope.workout_categories[index].name + '?')

    if (remove) {
      Http.post({ action: 'remove_workout_category', category_id: category_id }, function(response) {
        $scope.workout_categories.splice(index, 1);
        $scope.$apply();
      });
    }
  }

  $scope.editWorkout = function(index) {
    let category = $scope.workout_categories[index]
    $scope.workout = category;
    $scope.workout_index = index;
    $scope.edit_mode = true;
  }

  init();
}])
.controller('FMTAdminPackage', [ "$scope", "Http", function($scope, Http) {
  $scope.exercises = [];
  $scope.added_exercises = [];

  function init() {
    Http.get({ action: 'get_exercise' }, function(response) {
      $scope.exercises = JSON.parse(response);
      $scope.$apply();
    });

    Http.get({ action: 'get_packages' }, function(response) {
      $scope.packages = JSON.parse(response);
      $scope.$apply();
    });
  }

  $scope.create = function() {
    Http.post({ action: 'create_package', package: $scope.package, exercises: $scope.added_exercises }, function(response) {
      $scope.packages.push(JSON.parse(response)[0]);
      $scope.package = {}
      $scope.added_exercises = [];
      $scope.add = false;
      $scope.$apply();
    });
  }

  $scope.delete = function(index) {
    let package = $scope.packages[index]
    Http.post({ action: 'remove_package', package_id: package.id }, function(response) {
      $scope.packages.splice(index, 1)
      $scope.$apply();
    })
  }

  $scope.include = function(index) {
    $scope.added_exercises.push(angular.copy($scope.exercises[index]));
  }

  $scope.exclude = function(index) {
    $scope.added_exercises.splice(index, 1);
  }

  init();

}])
.controller('FMTExerciseCtrl', ["$scope", "Http", "fmturl", "$filter", function($scope, Http, fmturl, $filter) {
  $scope.exercises = [];
  $scope.exercise = { category: "selected", type: "free" };
  $scope.search_categories;
  $scope.cat_search = null;
  $scope.selected = [];
  
  function init() {
    Http.get({ action: 'get_exercise' }, function(response) {
      $scope.exercises = $filter('orderBy')(JSON.parse(response), ['name', 'category', '-created_at'])
      $scope.$apply();
    })

    Http.get({ action: 'get_categories' }, function(response) {
      $scope.categories = $filter('orderBy')(JSON.parse(response), ['name', '-created_at']);
      $scope.search_categories = $scope.categories;
      $scope.$apply();
    })
  }

  $scope.submit = function() {
    $scope.create();
  }

  $scope.createNew = function() {
    $scope.create();
  }

  $scope.create = function() {
    let exercise = $scope.exercise;
    delete exercise.id
    $scope.exercise.created_at = new Date().toISOString().slice(0, 19).replace('T', ' ');;
    if (exercise.name) {
      Http.post({ action: 'create_exercise', exercise: exercise }, function(response) {
        $scope.exercises.push(JSON.parse(response)[0])
        $scope.exercises = $filter('orderBy')($scope.exercises, '-created_at');
        $scope.exercise = { category: "selected", type: "free" }
        $scope.add = false;
        $scope.alertdisplay = true;
        $scope.edit_mode = false;
        $scope.$apply();
        $("#createexercise").modal("hide")
      });

      setTimeout(function() {
        $scope.alertdisplay = false;
        $scope.$apply();
      }, 2500);
    }
  }

  $scope.remove = function(index) {
    let exercises = [$scope.exercises[index].id];
    let remove = confirm('Are you sure you want to remove selected exercises?');
    if (remove) {
      Http.post({ action: 'remove_exercise', exercises: exercises }, function(response) {
        $scope.exercises.splice(index, 1);
        $scope.$apply();
      });
    }
  }

  $scope.editExercise = function(index) {
    let exercise = angular.copy($scope.exercises[index])
    $scope.edit = exercise;
    $scope.edited_index = index;
    jQuery("#editexercise").modal("show")
  }

  $scope.update = function() {
    Http.post({ action: 'update_exercise', exercise_id: $scope.edit.id, exercise: $scope.edit },
    function(response) {
      setTimeout(function() {
        $scope.edit_mode = false;
        $scope.exercises[$scope.edited_index] = $scope.edit;
        $scope.exercise = {};
        $scope.$apply();
        alert("Successfully update exercise");
        jQuery("#editexercise").modal("hide")
      }, 200);
    })
  }

  $scope.addCategory = function() {
    let data = {
      name: $scope.category.name,
    }
    Http.post({ action: 'create_category', category: data }, function(response) {
      $scope.category = {};
      $scope.edit_mode_cat = false;
      $scope.categories.push(JSON.parse(response)[0]);
      $scope.$apply();
    });
  }

  $scope.updateCategory = function() {
    let category = $scope.category;
    Http.post({ action: 'update_category', category: category, category_id: category.id }, function(response) {
      $scope.category = {};
      $scope.categories[$scope.edit_cat_index] = category;
      $scope.edit_mode_cat = false;
      $scope.$apply();
    });
  }

  $scope.removeCat = function(index) {
    let category = $scope.categories[index];
    let remove = confirm('Are you sure you want to remove this category ' + category.name);

    if (remove) {
      Http.post({ action: 'remove_category', category_id: category.id }, function(response) {
        $scope.categories.splice(index, 1);
        $scope.$apply();
      });
    }
  }

  $scope.editCat = function(index) {
    let category = angular.copy($scope.categories[index])
    $scope.category = category;
    $scope.edit_mode_cat = true;
    $scope.edit_cat_index = index;
  }

  $scope.$watch('cat_search', function(newval) {
    let categories = angular.copy($scope.categories);
    if (newval) {
      let result = categories.filter(function(category) {
        return category.name.toLowerCase().indexOf(newval.toLowerCase()) >= 0;
      });

      $scope.search_categories = result;
    } else {
      $scope.search_categories = categories;
    }
  })

  $scope.upload = function() {
    let uploader = wp.media({
      title: 'Select Thumbnail',
      button: {
        text: 'Use this thumbnail',
      },
      multiple: false,
    });

    uploader.open();

    uploader.on('select', function() {
      let attachment = uploader.state().get('selection').first().toJSON();
      $scope.exercise.thumbnail = attachment.url
      $scope.$apply();
    })
  }

  init();
}])
