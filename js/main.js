if (jQuery && !$) $ = jQuery;

function request( url, type, data, callBack ) {
    /*
    $.ajax({
        url         : localObj.ajaxurl,
        data        : { action: url, data: data },
        type        : type,
        success     : callBack
    });
    */
}

class FMTWorkout {

    constructor() {
        this.tempInputValue = "";

        this.exer_list = [];
        this.getExerciseList();

        this.staticData = [];
        this.addWorkout();
        this.addEditexercise();
        this.setupWorkout();
        this.setupExerSelect();
        this.setupIsIncluded();
        this.setupInputEdit();

        this.setupExercises();
        this.beginTraining();
        this.pauseTraning();
        this.resumeTraining();
        this.endTraining();

        this.exer_index     = -1;
        this.setTotalTime   = 0;
        this.setInterval    = null;
        this.isPaused       = false;
        this.countType      = "set_duration"; //set_duration, rest, break

        this.current_sets   = 0;
        this.current_rest   = 0;
        this.set_end_time   = 0;

        this.calculateTotalTime();
    }

    getExerciseList() {
        let self = this;
        request("fetch_exercise_list", "post", {}, function(res) {
            self.exer_list = JSON.parse(res);
            self.setupWorkoutDropdown();
        });
    }

    addEditexercise() {
        let self = this;
        this.calculateTotalTime();
        $('.add-exercise-trigger').on('click', function(){
            let data = {
                workout_id      : $('#fmt-workout-select').val(),
                list_index      : $('#exercises-table-body')[0].children.length,
                cycle_rate      : 0,
                set_duration    : 0,
                rest            : 0,
                sets            : 0,
                user_weight     : 0,
                break           : 0,
            };
            request("add_exercise_user", "post", data, function(res) {
                self.fetchWorkoutExercises( $("#fmt-workout-select").val() );
            });
        });
    }

    addWorkout() {
        let self = this;

        $("#add-workout").on("click", function(){
            self.toggleAddWorkoutModal();
        });
        $("#add-workout-modal-close").on("click", function(){
            self.toggleAddWorkoutModal();
        });
        $("#add-workout-btn").on("click", function(){
            self.submitWorkout();
        });
    }

    toggleAddWorkoutModal() {
        if ( $("#add-workout-modal").attr("data-status") == "open" ) $("#add-workout-modal").attr("data-status", "close");
        else $("#add-workout-modal").attr("data-status", "open");
    }

    submitWorkout() {
        let self = this;

        if ( !$("#workout-title").val() ) return false;

        let data = {
            workout_name    : $("#workout-title").val(),
            category_id     : $("#workout-category").val(),
            description     : $("#workout-description").val(),
        };

        request("add_workout", "post", data, function(res) {
            self.toggleAddWorkoutModal();       // close modal after submit
            self.setupWorkoutDropdown();
            $("#workout-title").val("");
            $("#workout-description").val("");
        });
    }

    changeSelectedWorkout( workout_id ) {
        let self = this;
        let data = {
            workout_id: workout_id
        };
        request("change_selected_workout", "post", data, function(res) {
            self.fetchWorkoutExercises( $("#fmt-workout-select").val() );
        });
    }

    setupWorkout() {
        let self = this;
        $('#fmt-workout-select').on('change', function(){
            self.changeSelectedWorkout( $(this).val() );
        });
    }

    setupWorkoutDropdown() {
        let self = this;
        this.fetchWorkouts(function( workouts ) {
            $("#fmt-workout-select").empty();
            $.each(workouts, function(i, v) {
                $("#fmt-workout-select").append(`<option ${v.is_selected == '1' ? 'selected' : '' } value="${v.workout_id}">${v.workout_name} - ${v.description}</option>`);
                if ( i == 0 ) {
                    $(".exers.hidden").removeClass("hidden");
                    $("#total-time").removeClass("hidden");
                }
            });

            self.fetchWorkoutExercises( $("#fmt-workout-select").val() );
        });
    }

    fetchWorkoutExercises( workout_id ) {
        let self = this;
        let data = {
            workout_id: workout_id
        };
        request("fetch_workout_exercises", "post", data, function(res) {
            res = JSON.parse(res)
            self.staticData = [];
            $.each( res, function(i, v) {
                self.staticData[i] = {
                    cycle_rate      : v.exers.cycle_rate,
                    set_duration    : v.exers.set_duration,
                    rest            : v.exers.rest,
                    sets            : v.exers.sets,
                    user_weight     : v.exers.user_weight,
                    exercise_break  : v.exers.break,
                    exercise_time   : self.setExerciseBreak( v.exers, 0 ).toFixed(2),
                    is_selected     : v.workout_exers.is_checked,
                    workout_exer_id : v.workout_exers.workout_exercise_id,
                    workout_exer    : v.workout_exers
                };
            });
            self.setupExercises();
        });
    }

    setExerciseBreak( exers, temp_break ) {
        return (parseFloat(exers.set_duration) + parseFloat(exers.rest) ) * parseFloat(exers.sets);
    }

    formatExerciseOptions(workout_exer) {
      let options = "<option>-- select exercise --</option>";
      let selectedImg = "";
      $.each(this.exer_list, function(index, value) {
        if (value.exercise_id==workout_exer.selected_exer_list) {
          selectedImg = value.image;
          options += `<option selected value="${value.exercise_id}">${value.exercise_name}</option>`;
        } else {
          options += `<option value="${value.exercise_id}">${value.exercise_name}</option>`;
        }
      });
      return {
        options: options,
        image: selectedImg
      };
    }

    setupExercises() {
        let self = this;
        $("#exercises-table-body").html('');
        $.each(this.staticData, function(i, v) {
            let selectedExerList = self.formatExerciseOptions(v.workout_exer);
            let is_selected = v.is_selected == "1" ? "checked" : "";
            $("#exercises-table-body").append(`
                <tr class="exer-index-${i} exers">
                    <td><input data-workout_exer_id="${v.workout_exer_id}" id="include-exer-${i}" class="is-included" ${is_selected} type="checkbox"></td>
                    <td>${i}</td>
                    <td>
                      <select data-workout_exer_id="${v.workout_exer_id}" data-index="${i}" class="exer-select">
                        ${selectedExerList.options}
                      </select>
                    </td>
                    <td><input data-exer_id="${v.workout_exer.exercise_id}" class="input-cycle-rate" type="text" value="${v.cycle_rate} sec"></td>
                    <td><input data-exer_id="${v.workout_exer.exercise_id}" class="input-set-duration" type="text" value="${v.set_duration} sec"></td>
                    <td><input data-exer_id="${v.workout_exer.exercise_id}" class="input-rest" type="text" value="${v.rest} sec"></td>
                    <td><input data-exer_id="${v.workout_exer.exercise_id}" class="input-sets" type="text" value="${v.sets}"></td>
                    <td><input data-exer_id="${v.workout_exer.exercise_id}" class="input-weight" type="text" value="${v.user_weight}"></td>
                    <td><a href="#" data-target="#videoModal" data-toggle="modal"><div class="disp-block exer-avatar" style="background: url(${selectedExerList.image ? selectedExerList.image : 'http://via.placeholder.com/45x45'});"></div></></td>
                    <td><input data-exer_id="${v.workout_exer.exercise_id}" type="text" class="input-break" value="${v.exercise_break} sec"></td>
                    <td>${v.exercise_time} sec.</td>
                    <td><button>=</button></td>
                </tr>

            `);
        });
        self.calculateTotalTime();
    }

    isNumber(input) {
      if ($.isNumeric($(input).val())) {
        return true;
      }
      $(input).val(this.tempInputValue);
      return false;
    }

    updateStaticData(exer_id, value, key) {
      let self = this;
      $.each(this.staticData, function(i, v) {
        if (v.workout_exer.exercise_id == exer_id) {
          self.staticData[i][key] = value;
        }
      });
    }

    setupInputEdit() {
      let self = this;
      $(document).on('focus', '.input-cycle-rate', function() {
        self.tempInputValue = $(this).val();
        $(this).val('');
      });
      $(document).on('focus', '.input-set-duration', function() {
        self.tempInputValue = $(this).val();
        $(this).val('');
      });
      $(document).on('focus', '.input-rest', function() {
        self.tempInputValue = $(this).val();
        $(this).val('');
      });
      $(document).on('focus', '.input-sets', function() {
        self.tempInputValue = $(this).val();
        $(this).val('');
      });
      $(document).on('focus', '.input-weight', function() {
        self.tempInputValue = $(this).val();
        $(this).val('');
      });
      $(document).on('focus', '.input-break', function() {
        self.tempInputValue = $(this).val();
        $(this).val('');
      });

      $(document).on('blur', '.input-cycle-rate', function() {
        if (self.isNumber(this)) {
          let exer_id = $(this).attr('data-exer_id');
          let value   = $(this).val();
          let key     = 'cycle_rate';
          request("update_workout_exercises_input", "post", {exer_id: exer_id, cycle_rate: value}, function(res){ 
            self.updateStaticData(exer_id, value, key); 
          });
        }
      });
      $(document).on('blur', '.input-set-duration', function() {
        if (self.isNumber(this)) {
          let exer_id = $(this).attr('data-exer_id');
          let value   = $(this).val();
          let key     = 'set_duration';
          request("update_workout_exercises_input", "post", {exer_id: exer_id, set_duration: value}, function(res){ 
            self.updateStaticData(exer_id, value, key); 
          });
        }
      });
      $(document).on('blur', '.input-rest', function() {
        if (self.isNumber(this)) {
          let exer_id = $(this).attr('data-exer_id');
          let value   = $(this).val();
          let key     = 'rest';
          request("update_workout_exercises_input", "post", {exer_id: exer_id, rest: value}, function(res){ 
            self.updateStaticData(exer_id, value, key); 
          });
        }
      });
      $(document).on('blur', '.input-sets', function() {
        if (self.isNumber(this)) {
          let exer_id = $(this).attr('data-exer_id');
          let value   = $(this).val();
          let key     = 'sets';
          request("update_workout_exercises_input", "post", {exer_id: exer_id, sets: value}, function(res){ 
            self.updateStaticData(exer_id, value, key); 
          });
        }
      });
      $(document).on('blur', '.input-weight', function() {
        if (self.isNumber(this)) {
          let exer_id = $(this).attr('data-exer_id');
          let value   = $(this).val();
          let key     = 'user_weight';
          request("update_workout_exercises_input", "post", {exer_id: exer_id, user_weight: value}, function(res){ 
            self.updateStaticData(exer_id, value, key); 
          });
        }
      });
      $(document).on('blur', '.input-break', function() {
        if (self.isNumber(this)) {
          let exer_id = $(this).attr('data-exer_id');
          let value   = $(this).val();
          let key     = 'rest';
          request("update_workout_exercises_input", "post", {exer_id: exer_id, break: value}, function(res){ 
            self.updateStaticData(exer_id, value, key); 
          });
        }
      });
    }

    setupIsIncluded() {
      $(document).on('change', '.is-included', function() {
        let data          = {
          workout_exercise_id : $(this).attr('data-workout_exer_id'),
          is_checked          : $(this)[0].checked
        };
        request("update_workout_exercises_is_checked", "post", data, function(res){ });
      })
    }

    setupExerSelect() {
      let self = this;
      $(document).on('change', '.exer-select', function() {
        let workout_id    = $('#fmt-workout-select').val();
        let exer_select   = $(this).val();
        let data          = {
          workout_exercise_id : $(this).attr('data-workout_exer_id'),
          selected_exer_list  : exer_select
        };
        let avatar        = self.exer_list.filter(function(item) {
          return exer_select == item.exercise_id;
        });
        $('.exer-index-' + $(this).attr('data-index') + ' .exer-avatar').css({'background': `url(${avatar[0]['image']})`});
        request("update_workout_exercises_selected_exer", "post", data, function(res){ });
      });
    }

    beginTraining() {
      let self = this;
      $(".begin-training").on("click", function(){
        if ( self.setInterval == null ) {
          self.startCountdown();
        }
      });
    }

    pauseTraning() {
      let self = this;
      $(".controls #pause-training").on("click", function(){
        if ( self.setTotalTime > 0 ) {
          self.isPaused = true;
        }
      });
    }

    resumeTraining() {
      let self = this;
      $(".controls #resume-training").on("click", function(){
        if ( self.setTotalTime > 0 ) {
          self.isPaused = false;
        }
      });
    }

    endTraining() {
        let self = this;
        $("#end-training").on("click", function(){
            $(".modal").modal("hide");
            self.doEndTraining();
        });
    }

    doEndTraining() {
        let self = this;
        clearInterval(self.setInterval);
        self.exer_index       = -1;
        self.setTotalTime     = 0;
        self.setInterval      = null;
        self.isPaused         = false;
        $(".label").html( "Set Duration" );
        $(".time-remaining").html( "00:00" );
        $(".sets-number").html( "" );
        $(".exers").removeClass("active");
    }

    startCountdown() {
        let interval = 10;
        let self = this;
        this.setInterval = setInterval(function() {
            if (self.countType == "set_duration") {
                self.doSetDurationCountdown(interval);
            } else if ( self.countType == "rest" ) {
                self.doRestCountdown(interval);
            } else if ( self.countType == "exercise_break" ) {
                self.doExerciseBreakCountdown(interval);
            }
        }, interval);
    }

    doSetDurationCountdown(interval) {
        let self = this;
        if ( self.current_sets == 0 ) {
            self.exer_index += 1;
            if ( !$("#include-exer-" + self.exer_index).is(":checked") ) {
                self.doSetDurationCountdown(interval);
            }
            if (self.staticData[self.exer_index]) {
                self.current_sets           = self.staticData[self.exer_index].sets;
                self.current_set_duration   = self.staticData[self.exer_index].set_duration;
                $(".exers").removeClass("active");
                $(".exer-index-" + self.exer_index).addClass("active");
            }
        }
        if ( !self.isPaused ) {
            $(".label").html( "Set Duration" );
            $(".time-remaining").html( (self.current_set_duration - (self.setTotalTime / 1000)).toFixed(2) );
            $(".sets-number").html( self.current_sets );
            self.setTotalTime += interval;
        }
        if ( self.current_set_duration - (self.setTotalTime / 1000) <= 0 ) {
            if (self.staticData.length - 1 == self.exer_index) {
                self.doEndTraining();
            } else {
                self.setTotalTime     = 0;
                self.countType        = "rest";
                if ( self.current_sets == 1 ) {
                    self.countType = "exercise_break";
                }
            }
        }
    }

    doRestCountdown(interval) {
        let self = this;
        if (self.staticData[self.exer_index]) {
            self.current_rest = self.staticData[self.exer_index].rest;
            $(".label").html( "Rest" );
            $(".time-remaining").html( (self.current_rest - (self.setTotalTime / 1000)).toFixed(2) );
            self.setTotalTime += interval;
            if ( self.current_rest - (self.setTotalTime / 1000) <= 0 ) {
                self.current_sets--;
                self.setTotalTime   = 0;
                self.countType      = "set_duration";
            }
        }
    }

    doExerciseBreakCountdown(interval) {
        let self = this;
        $(".label").html( "Exercise Break" );
        $(".time-remaining").html( (self.staticData[self.exer_index].exercise_break - (self.setTotalTime / 1000)).toFixed(2) );
        self.setTotalTime += interval;
        if ( self.staticData[self.exer_index].exercise_break - (self.setTotalTime / 1000) <= 0 ) {
            self.current_sets     = 0;
            self.setTotalTime     = 0;
            self.countType        = "set_duration";
        }
    }

    fetchWorkouts( callBack ) {
        let data = { };
        request( "get_workouts", "post", data, function( res ){
            callBack(JSON.parse(res));
        });
    }

    calculateTotalTime() {
        let totalTime = 0;
        $.each(this.staticData, function(i, v) {
            totalTime += (((v.set_duration * v.sets) + (v.rest * (v.sets - 1)) + (v.exercise_break * v.sets))); 
        });
        $("#total-time span").html( (totalTime / 60).toFixed(2) );
        let newTotalTime = 0;
        $.each(this.staticData, function(i, v) {
            let time = (parseFloat(v.set_duration) * parseFloat(v.sets)) + ((parseFloat(v.rest) * parseFloat(v.sets)) - parseFloat(v.rest)) + parseFloat(v.exercise_break);
            newTotalTime += parseFloat(time);
            totalTime += ((v.set_duration * v.sets) + (v.rest * (v.sets - 1)) + (v.exercise_break * v.sets));
        });
        $("#total-time span").html( (newTotalTime / 60).toFixed(2) );
    }

} new FMTWorkout();

class FMTAdmin {
    
  constructor() {
    this.packageExers = [];
    this.setup();
    this.addExercise();
    this.updateExercise();
    this.removeExercise();
    this.fetchExercises();
    this.setupUploadAvatar();
    this.onSelectPackageExercise();
    this.onCreatePackage();
    this.getPackages();
    this.deletePackages();
    this.getAudios();
    this.deleteWorkout();
  }

  getPackages() {
    if ($('#package-list').length) {
        request("get_packages", "post", {}, function(res) {
            res = JSON.parse(res);
            $.each(res, function(index, value) {
              $('#package-list').append(`
                <tr>
                  <td style="20%"><input type="checkbox" value="${value.package_id}" class="pkgcheckbox"/></td>
                  <td>${value.package_name}</td>
                  <td>${value.package_price}</td>
                  <td>${JSON.parse(value.package_exercise_list).length}</td>
                </tr>
              `);
            });
        });
    }
  }

  deleteWorkout() {
    $('#delete-workout').click(function() {
        let id = $("#fmt-workout-select").val();
        if (id) {
            if (confirm("Are you sure you want to delete this workout?")) {
                request("delete_workout", "post", {workout_id: id}, function(res) {
                    window.location.reload();
                });
            }
        }
    });
  }

  deletePackages() {
    $("#deletepkg").click(function() {
      let packages = [];
      $(".pkgcheckbox:checked").each((index, node) => {
          packages.push(node.value);
          $(node).parent().parent().remove();
      })

      request("delete_package", "post", packages, (response) => {
        console.log('Package(s) has been deleted'); 
      });
    }); 
  }

  onCreatePackage() {
    $('#create-package').click(() => {
        let data = {
            package_name: $('#package-name').val(),
            package_price: $('#package-price').val(),
            package_description: $('#package-description').val(),
            package_exercise_list: JSON.stringify(this.packageExers.map(item => {
                return parseInt(item.id);
            }))
        };

        request("create_package", "post", { package: data }, function(res) {
            console.log(res);
        });
    });
  }

  onSelectPackageExercise() {
    this.packageExers = [];
    $('#select-package-exer').click(() => {
        $('.package-exer-checkbox:checked').each((key, item) => {
            this.packageExers.push({ id: item.value, name: $(item).attr('data-name') });
            $('#package-exer-modal').css({'display': 'none'});
        });
    });
  }

  fetchExercises() {
    let link = $('#add-exer-link').attr('href');
    if ( $('#fmt-admin-exer-list').length ) {
      $('#fmt-admin-exer-list').html('');
      request("fetch_exercise_list", "post", {}, function(res) {
        res = JSON.parse(res);
        $.each(res, function(index, value) {
          $('#fmt-admin-exer-list').append(`
            <tr>
              <td style="20%"><input type="checkbox" class="fmtcheckbox" value="${value.exercise_id}"/></td>
              <td>${value.exercise_name}</td>
              <td>${value.category_name}</td>
              <td>
                  <a href='${link}&name=${value.exercise_name}&category=${value.category_id}&desc=${value.description}&edit=true&id=${value.exercise_id}'>
                    Edit
                  </a>
              </td>
            </tr>
          `);
        });
      });
    }
    if ( $('#package-exer-list').length ) {
      $('#package-exer-list').html('');
      request("fetch_exercise_list", "post", {}, function(res) {
        res = JSON.parse(res);
        $.each(res, function(index, value) {
          $('#package-exer-list').append(`
            <tr>
              <td><input type="checkbox" class="package-exer-checkbox" data-name="${value.exercise_name}" value="${value.exercise_id}"></td>
              <td>${index + 1}</td>
              <td>${value.exercise_name}</td>
              <td>${value.category_name}</td>
            </tr>
          `);
        });
      });
    }
  }

  removeExercise() {
    $("#deletebtn").click(function() {
      let ids = [];
      $(".fmtcheckbox:checked").each(function(index, node) {
        ids.push(parseInt(node.value))
      })

      request("remove_workout", "post", { ids: ids }, function(res) {
        $(".fmtcheckbox:checked").each(function(index, node) {
          $(node).parent().parent().remove();
        })
      });
    });
  }

  addExercise() {
    $("#create-exercise-button").on("click", function(){
      let data = {
        exercise_name   : $("#exer-name").val(),
        description     : $("#exer-description").val(),
        category_id     : $("#exer-category").val(),
        avatar          : $("#exer-avatar").val()
      };
      request("add_exercise", "post", data, function(res) {
        window.location = $("#create-exercise-button").attr('data-redirect');
      });
    });
  }

  updateExercise() {
    $("#update-exercise-btn").click(function() {
      let data = {
        exercise_name   : $("#exer-name").val(),
        description     : $("#exer-description").val(),
        category_id     : $("#exer-category").val()
      };
      let exercise_id = $("#exercise_id").val();

      request("update_workout", "post", { data: data, id: exercise_id }, function(res) {
        window.location = $("#update-exercise-btn").attr('data-redirect');
      });
    })
  }

  setupUploadAvatar() {
    let fileUploader = wp.media({
      title: 'Select Image',
      button: {
        text: 'Use this image'
      },
      mutiple: false
    });

    $('#upload-avatar').on('click', function() {
      fileUploader.open();
    });

    if ( fileUploader ) {
      fileUploader.on('select', function() {
        let attachment = fileUploader.state().get('selection').first().toJSON();
        $('#exer-avatar').val(attachment.url);
      });
    }
  }

  setup() {
    let audioUploader = wp.media({
      title: 'Select Audio',
      button: {
        text: 'Use this audio'
      },
      mutiple: false
    });

    $('#upload-audio').on('click', function() {
      audioUploader.open();
    });

    if ( audioUploader ) {
      audioUploader.on('select', function() {
        let attachment = audioUploader.state().get('selection').first().toJSON();
        let data = {
            url: attachment.url,
            name: attachment.filename
        };
        request("create_audio", "post", data, function(res) { });
      });
    }
  }

  getAudios() {
    if ($("#fmt-setup-page").length) {
        request("get_audios", "post", {}, function(res) {
            res = JSON.parse(res);
            console.log(res);
        });
    }
  }
} new FMTAdmin();
