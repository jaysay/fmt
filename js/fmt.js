Array.prototype.move = function(from, to) {
  this.splice(to, 0, this.splice(from, 1)[0])
  return this;
}
let fmt_app = angular.module('fmt-app', ['datatables'])
.filter('time', function() {
  return function(val) {
    let date = new Date(val)
    return (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getFullYear();
  }
})
.constant('fmturl', localObj.ajaxurl)
.factory('Speech', function() {
  let speech = this;
  
  const synth = window.speechSynthesis
  
  speech.talk = function(word, callback) {
    const utterThis = new SpeechSynthesisUtterance(word)
    utterThis.pitch = 1
    utterThis.rate = .90
    synth.speak(utterThis)
    
    utterThis.onend = function() {
      if (callback) {
        callback();
      }
    }
  }

  return speech;
})
.factory('StateHandler', function($sce) {
	let handler = this;

	handler.init = function(routines) {
		let routine = routines[0];
    if (routine.exercise && routine.exercise.animation_link) {
      routine.exercise.animation_media = $sce.trustAsResourceUrl(routine.exercise.animation_link + '?autoplay=1&loop=1');
    }
		return {
			next_index: 1,
			current_index: 0,
			remaining_sets: routine.sets, // minus 1
			remaining_rest: routine.sets - 1,
			routine: routines[0],
			routines: routines,
		}
	}

	handler.set = function(state) {
    if (state && state.remaining_sets > 0) {
        state.remaining_sets -= 1;
        return state;
    }
	}

	handler.rest = function(state) {
    if (state.remaining_sets > 0) {
        state.remaining_rest -= 1;
        return state;
    }
	}

	handler.next = function(state) {
    if (!state) return false;
		let routine = state.routines[state.next_index]
    if (routine && routine.exercise.animation_link) {
      routine.exercise.animation_media = $sce.trustAsResourceUrl(routine.exercise.animation_link + '?autoplay=1&loop=1');
    }
		if (routine) {
			state.next_index = state.next_index + 1;
			state.current_index = state.next_index;
			state.remaining_sets = routine.sets
			state.remaining_rest = routine.sets - 1
			state.routine = routine 
      state.is_next = true;
			return state;
		}
		return false;
	}

  handler.pause = function(state, seconds, milliseconds) {
    state.timer = {
      seconds: parseInt(seconds),
      milliseconds: parseInt(milliseconds),
    }
    return state
  }

	return handler
})
.factory('Http', function() {
  let http = this;

  http.post = function(url, data, callback) {
    jQuery.ajax({
      url: url,
      type: 'post',
      data: data,
      success: callback
    })
  }

  http.get = function(url, data, callback) {
    jQuery.ajax({
      url: url,
      type: 'get',
      data: data,
      success: callback
    })
  }

  return http;
})
.factory('NTimer', function($interval) {
  let timer = this;
  timer.lapsed = 0
  timer.started_at = 0

  timer.start = function(set, cycle_rate, callback, completion_callback) {
    cycle_rate = cycle_rate ? cycle_rate : 0;
    let that = this;
    let start_time = new Date()
    let cyclerate = cycle_rate //getEvenCycleRate(cycle_rate);
    let lap = (cyclerate * 1000)
    that.started_at = set
    
    let interval = $interval(function() {
      let lapsed = new Date() - start_time
      let result = (lapsed / 1000).toFixed(2)
      let seconds = Math.round(result)
      that.lapsed = lapsed

      if (result >= set) {
        console.log('---completed')
        $interval.cancel(interval)
        setTimer(0, 0)
        callback()
        return
      }

      if ((set - 2) == result) {
        if (completion_callback) {
          completion_callback();
        }
      }

      let laps = (lap / 1000).toFixed(2)
      let range = (parseFloat(laps) + parseFloat(.01)).toFixed(2)
      let range1 = (parseFloat(laps) + parseFloat(.02)).toFixed(2)
      
      if (result === laps || result == range || result == range1) {
        console.log('---cycle rate ' + result)
        
        if (cycle_rate) {
          document.getElementById("audio").play()
          lap += (cyclerate * 1000)
        }
      }

      setTimer(result, set)
    }, 10);

    return interval
  }

  timer.displayTime = function(time, set) {
    setTimer(time, set)
  }

  function getEvenCycleRate(cyclerate) {
    let num = cyclerate.toString().split(".")
    let seconds = num[0]
    let milliseconds = parseInt(num[1])
    if (milliseconds <= 9) {
      let result = (milliseconds % 2) === 0 ? milliseconds : milliseconds + 1;
      return parseFloat(seconds + '.' + (result * 10))
    } else {
      let roundToTen = Math.ceil(milliseconds / 10) * 10;
      let nearestMillisecond = roundToTen > milliseconds ? roundToTen - 10 : roundToTen
      let firstDigit = parseInt(nearestMillisecond.toString().replace(/0/,''))
      let result = (firstDigit % 2) === 0 ? firstDigit : firstDigit + 1;
      return parseFloat(seconds + '.' + (result * 10))
    }
  }

  function setTimer (time, set, callback) {
    let result = (set - time).toFixed(2)
    let timer = result.split('.')
    let sec = timer[0] <= 9 ? '0' + Math.abs(timer[0]) : timer[0];
    jQuery("div#seconds").text(sec);
    jQuery("div#milliseconds").text(timer[1]);
    if (callback) {
      return callback(sec, timer[1])
    }
  }

  return timer;
})
.controller('WorkoutCtrl', ["$scope", "Http", "fmturl", "StateHandler", "Speech", "$sce", "$filter", "$window", "$timeout", "$interval", "NTimer", "$rootScope",
function($scope, Http, fmturl, StateHandler, Speech, $sce, $filter, $window, $timeout, $interval, NTimer, $rootScope) {

  $scope.exercises = [];
  $scope.workouts = [];
  $scope.workout_routine = [];
  $scope.select_workout = true;
  $scope.interval = null;
  let timer = NTimer
    
  function init() {
    Http.post(fmturl, { action: 'get_exercise' }, function(response) {
      $scope.exercises = JSON.parse(response)
      $scope.$apply();
    });

    Http.post(fmturl, { action: 'get_workout' }, function(response) {
      let data = JSON.parse(response);
      data = data ? data : [];
      $scope.workouts = $filter('orderBy')(data, ['name', 'category', '-created_at'])
      $scope.workout_source = $filter('orderBy')(data, ['name', 'category', '-created_at'])
      $scope.media(0);
      $scope.$apply();
    });

    Http.get(fmturl, { action: 'get_workout_categories' }, function(response) {
      let data = JSON.parse(response);
      let categories = data ? data : [];
      $scope.categories = $filter('orderBy')(categories, ['name']);
      $scope.$apply();
    });
  }
  
  /*
   * Workout Methods
   */
  $scope.$watch('workoutsearch', function(newval) {
    let source = angular.copy($scope.workout_source);
    let result = source;
    if (newval) {
      result = source.filter(function(workout) {
        return workout.name.toLowerCase().indexOf(newval.toLowerCase()) >= 0;
      });
    }

    $scope.workouts = result;
  });

  $scope.$watch('sortby', function(sort) {
    let sort_by = sort === "created_at" ? "-created_at" : sort;
    $scope.workouts = $filter('orderBy')($scope.workouts, [sort_by])
  })

  $scope.$watch('exercise_search', function(newval) {
    let source = angular.copy($scope.exercise_source);
    if (newval) {
      let result = source.filter(function(exercise) {
        let data = exercise.exercise;
        return data && data.name.toLowerCase().indexOf(newval.toLowerCase()) >= 0;
      });

      return $scope.workout_routine = result;
    }

    return $scope.workout_routine = source;
  });

  /* create workout */
  $scope.create = function() {
    let workout = $scope.workout;
    Http.post(fmturl, { action: 'create_workout', workout: workout }, function(response) {
      $scope.workouts.push(JSON.parse(response)[0]);
      $scope.workout_source.push(JSON.parse(response)[0]);
      $scope.workout = {};
      $scope.workouts = $filter('orderBy')($scope.workouts, ['+name', '-created_at'])
      $scope.create_workout = false;
      $scope.$apply();
      jQuery("#createworkout").modal("hide")
    });
  }

  /* update workout */
  $scope.update = function() {
    let updated_workout = {
      name        : $scope.workout.name,
      description : $scope.workout.description,
      category    : $scope.workout.category,
    }
    
    let workout = {
      action  : 'update_workout',
      workout : updated_workout,
      id      : workout.id,
    }

    Http.post(fmturl, workout, function(response) {
      $scope.workout = {};
      $scope.workout_edit_mode = false;
      $scope.$apply();
      alert('Successfully updated workout');
    })
  }

  /* show create workout modal */
  $scope.showCreateWorkout = function() {
    jQuery("#createworkout").modal("show");
    $scope.workout = {};
    $scope.workout_edit_mode = false;
  }
  
  $scope.setup = function(index) {
    let workout = $scope.workouts[index];
    $scope.workout_index = index;
    $scope.workout = workout
    Http.get(fmturl, { action: 'get_workout_routine', workout_id: workout.id }, function(response) {
      let routine = JSON.parse(response)
      $scope.workout_routine = routine.map(function(data) {
        data.exercise_time = calcExerciseTime(data);
        if (data.exercise && data.exercise.media_link) {
          data.exercise.media_link = $sce.trustAsResourceUrl(data.exercise.media_link + '?autoplay=1&loop=1');
        }
        return data
      });
      $scope.workout_routine = $filter('orderBy')($scope.workout_routine, ['-created_at'])
      $scope.exercise_source = $scope.workout_routine;
      $scope.select_workout = false;
      calTotalWorkoutTime($scope.workout_routine)
      $scope.$apply();
    })
  }

  $scope.editworkout = function(index) {
    $scope.workout_index = index;
    $scope.workout_edit_mode = true;
    $scope.workout = $scope.workouts[index]
    jQuery("#createworkout").modal("show");
  }

  $scope.removeWorkout = function(index) {
    let workout = $scope.workouts[index];
    let remove = confirm('Are you sure you want to remove workout?')
    if (remove) {
      Http.post(fmturl, { action: 'remove_workout', workout_id: workout.id }, function(response) {
        $scope.workouts.splice(index, 1);
        $scope.workout = {};
        $scope.$apply();
      });
    }
  }

  $scope.cancel = function() {
    $scope.workout_edit_mode = false;
    $scope.workout = {}
  }

  $scope.updateWorkout = function(index) {
    let updated_workout = {
      name: $scope.edit_workout.name,
      description: $scope.edit_workout.description,
      category: $scope.edit_workout.category,
    }
    
    let workout = {
      action: 'update_workout',
      workout: updated_workout,
      id: $scope.edit_workout.id,
    }

    Http.post(fmturl, workout, function(response) {
      $("#editworkout").modal("hide")

      setTimeout(function() {
        alert('Successfully updated workout');
      }, 200);
    })
  }

  /* Routine Methods */
  $scope.addRoutine = function() {
		let routine = {
      cycle_rate: 3,
      set_duration: 60,
      rest: 30,
      sets: 3,
      weight: 200,
      break: 120,
			workout_id: $scope.workout.id,
		}

		Http.post(fmturl, { action: 'create_routine', routine : routine }, function(response) {
			let routine = JSON.parse(response)[0]
			routine.exercise_time = calcExerciseTime(routine);	
      $scope.workout_routine.unshift(routine);
			$scope.$apply();
		});

		calTotalWorkoutTime();
  }

  $scope.removeRoutine = function(index) {
    let routine = $scope.workout_routine[index]
    let remove = confirm('Are you sure you want to remove this Exercise?');
    if (remove) {
      Http.post(fmturl, { action: 'remove_routine', routine_id: routine.id }, function(response) {
        $scope.workout_routine.splice(index, 1) 
        $scope.$apply();
      });
    }
  }

  $scope.edit = function(index) {
    let routines = $scope.workout_routine;
    $scope.workout_routine = routines.map(function(routine, key) {
      routine.edit_mode = false;
      if (key == index) {
        routine.edit_mode = true;
      }
      return routine;
    });
    $scope.edit_index = index;
    $scope.routine = routines[index];
  }

  $scope.save = function(index) {
		let routine = $scope.workout_routine[index];
    routine.edit_mode = false;
		routine.exercise_time = calcExerciseTime(routine);
		let update_routine = formRoutine(routine);
		Http.post(fmturl, { action: 'update_routine', routine_id: routine.id, routine: update_routine });
    jQuery("#editExercise").modal("hide");
  }

	$scope.include = function(index) {
		let routine = angular.copy($scope.workout_routine[index]);
		$scope.workout_routine[index].include = !routine.include;
	}

  $scope.selectWorkout = function() {
    $scope.select_workout = true;
    $scope.workout_routine = $scope.workout_routine.map(function(routine) {
      routine.include = false;
      return routine;
    });
  }

  $scope.createWorkoutCategory = function() {
    $scope.category = {};
    $scope.editing_category = false;
  }

	$scope.begin_training = function() {
		let routines = $scope.workout_routine.filter(function(routine) {
				return routine.include
		});
    $rootScope.routines = routines
    $scope.state = StateHandler.init(routines)
    $scope.action = 'set'
  }

  $scope.openMedia = function(index) {
		let routine = $scope.workout_routine[index]
    routine.exercise.media_link = $sce.trustAsResourceUrl(routine.exercise.media_link + '?autoplay=1&loop=1');
    $scope.selected_routine = routine;
    jQuery("#media").modal("show");
  }

  $scope.animationLink = function(index) {
		let routine = $scope.workout_routine[index]
    routine.exercise.animation_link = $sce.trustAsResourceUrl(routine.exercise.animation_link + '?autoplay=1&loop=1');
    $scope.selected_routine = routine;
    jQuery("#animation").modal("show");
  }

  jQuery("#media").on("hidden.bs.modal", function() {
    $scope.selected_routine = {};
  });

  /* Email and Print Methods */
  $scope.print = function() {
    let workout = $scope.fmt_workout; 
    Http.get(fmturl, { action: 'get_workout_routine', workout_id: workout.id }, function(response) {
      let print = JSON.parse(response)

      let header = "<h2>"+ workout.name +"</h2>";
      let start = "<table border='.5'>";
      let end = "</table>";
      let thead = "<thead><tr><th>Exercise Name</th><th>Cycle Rate</th><th>Set Duration</th><th>Rest</th><th>Set</th><th>Weight</th><th>Exercise Break</th><th>Exercise Time</th></tr></thead>";
      let tbody = "<tbody>";
      let tbody_end = "</tbody>";

      for (var i = 0; i < print.length; i++) {
        tbody += "<tr>" + 
                  "<td>" + print[i].exercise.name + "</td>" +
                  "<td>" + print[i].cycle_rate + "</td>" +
                  "<td>" + print[i].set_duration + "</td>" +
                  "<td>" + print[i].rest + "</td>" +
                  "<td>" + print[i].sets + "</td>" +
                  "<td>" + print[i].weight + "</td>" +
                  "<td>" + print[i].break + "</td>" +
                  "<td>8 min</td>" +
                "</tr>";
      }

      let el = header + start + thead + tbody + tbody_end + end;
      
      setTimeout(function() {
        let w = window.open("", "", "width=1000,height=1200,toolbars=no,scrollbar=yes,status=no,resizable=yes");
        w.document.writeln(el)
        w.document.close();
        w.focus()
        w.print()
        w.close()
      }, 500);

    });
  }

  $scope.media = function(index) {
    $scope.fmt_workout = $scope.workouts[index]
  }

  /**
   * Category Methods
   */
  $scope.editCategory = function(index) {
    $scope.category = $scope.categories[index];
    $scope.category_index = index;
    $scope.editing_category = true;
  }
  
  $scope.removeCategory = function(index) {
    let category = angular.copy($scope.categories[index]);
    let remove = confirm('Are you sure?, you can\'t undo this operation');
    if (remove) {
      Http.post(fmturl, { action: 'remove_workout_category', category_id: category.id });
      $scope.categories.splice(index, 1);
    }
  }

  $scope.updateCategory = function() {
      let category = $scope.category;
      Http.post(fmturl, { action: 'update_workout_category', category: category, category_id: category.id }, function(response) {
        $scope.categories[$scope.category_index] = category;
        $scope.category = {}
        $scope.editing_category = false;
        $scope.$apply();
        alert('Successfully updated category');
      });
  }

  $scope.createCategory = function() {
      if ($scope.category.name) {
        Http.post(fmturl, { action: 'create_workout_category', category: $scope.category }, function(response) {
          $scope.categories.push(JSON.parse(response)[0]);
          $scope.category = {}
          $scope.$apply();
        });
      }
  }

  $scope.closeModal = function() {
    $("#myModal").modal("hide");
    $scope.stop_timer();
  }

  $scope.moveUp = function(index, down) {
    let arr = $scope.workout_routine;
    let move_to = down ? index + 1 : index - 1;
    move_to = move_to < 0 ? arr.length - 1 : (move_to > (arr.length - 1) ? 0 : move_to);
    let result = $scope.workout_routine.move(index, move_to);
  }

	function calcExerciseTime(routine) {
		let sets = parseInt(routine.sets)
		let rest = parseInt(routine.rest)
		let exer_break = parseInt(routine.break)
		let set_duration = parseInt(routine.set_duration)

    let total = (set_duration * sets) + (rest * (sets - 1)) + exer_break;
		return (total / 60).toFixed(2);
	}

	function calTotalWorkoutTime() {
		let routines = $scope.workout_routine;
		let total = 0;
		routines.forEach(function(routine) {
			total += parseInt(calcExerciseTime(routine));
		})
		
		$scope.total_workout_time = total;
	}

	function formRoutine(routine) {
		return {
			cycle_rate: routine.cycle_rate,
			set_duration: routine.set_duration,
			rest: routine.rest,
			sets: routine.sets,
			weight: routine.sets,
			break: routine.break,
			exercise_id: routine.exercise.id,
		}
	}

  jQuery("#createworkout").on("hide.bs.modal", function() {
    $scope.workout = {};
    $scope.workout_edit_mode = false;
  });

  //$("#myModal").modal("show")

  $scope.state = null;
  $scope.action = null;

  $scope.$watch('action', function(action, oldAction) {
    if (action) {
      $scope.timer_next_state = action
      switch (action) {
        case 'set':
          $scope.state = StateHandler.set($scope.state);
          $scope.timer_state = $scope.state
          startSet(oldAction)
          break
        case 'rest':
          $scope.state = StateHandler.rest($scope.state);
          $scope.timer_state = $scope.state
          $scope.disabled_btns = true;
          startRest()
          break
        case 'break':
          $scope.state = StateHandler.next($scope.state);
          if ($scope.state) { $scope.timer_state = $scope.state }
          $scope.disabled_btns = true;
          startBreak()
          break;
        case 'stop':
          stopTimer()
          break;
      }
    }
  })

  $scope.pause_timer = function() {
    $interval.cancel($scope.interval)
    $scope.timeleft = timer.lapsed
    $scope.timer_paused = true;
  }

  $scope.resume_timer = function() {
    $interval.cancel($scope.interval)
    let start_at = (timer.started_at - ($scope.timeleft / 1000).toFixed(2))
    let state = $scope.state
    let exercise = state.routine
    let next_action = state.remaining_sets === 0 && state.remaining_rest === 0 ? 'break' : 'rest'
    $scope.interval = timer.start(start_at, parseFloat(exercise.cycle_rate), function() {
      $interval.cancel($scope.interval)
      $scope.action = next_action
    })
    $scope.timer_paused = false;
  }

  $scope.replay_timer = function() {
    $interval.cancel($scope.interval)
    $scope.state = StateHandler.init($rootScope.routines)
    $scope.stop_timer()

    $scope.disabled_replay_btn = true;
    $scope.disabled_btns = true;
    $scope.disabled_stop = true;
    $scope.timer_paused = false;
    $timeout(function() {
      $scope.action = 'set'
    }, 500)

    $timeout(function() {
      $scope.disabled_replay_btn = false;
      $scope.disabled_btns = false;
      $scope.disabled_stop = false;
    }, 8000)
  }

  $scope.stop_timer = function() {
    $interval.cancel($scope.interval)
    timer.displayTime(0, 0)
    $scope.timer_paused = false;
    $scope.disabled_btns = true;
    $scope.action = 'stop'
  }

  function startSet (oldAction) {
    $interval.cancel($scope.interval)
    reset()
    let state = $scope.state
    let exercise = state.routine
    let next_action = state.remaining_sets === 0 && state.remaining_rest === 0 ? 'break' : 'rest'
    let constraints = {
      throttle: state.is_next ? 3800 : 2800,
      message: state.is_next ? 'New exercise, ready, set ,start training' : 'Ready, set, start training',
    }

    $scope.disabled_btns = true;
    $scope.disabled_stop = true;
    $scope.disabled_replay_btn = true;

    if (oldAction != 'rest') {
      Speech.talk(constraints.message)
    } else {
      constraints.throttle = 100;
    }

    $timeout(function() {
      $scope.interval = timer.start(parseInt(exercise.set_duration), parseFloat(exercise.cycle_rate), function() {
        $interval.cancel($scope.interval)
        $scope.action = next_action
      })
      $scope.disabled_btns = false;
      $scope.disabled_stop = false;
      $scope.disabled_replay_btn = false;
    }, constraints.throttle)

    if (state.is_next) { state.is_next = false }
  }

  function startRest () {
    let state = $scope.state
    let exercise = state.routine
    $scope.interval = timer.start(parseInt(exercise.rest), 0, function() {
      $interval.cancel($scope.interval)
      $scope.action = 'set'
    }, function() {
      Speech.talk('Ready, set, start training')
    })
  }

  function startBreak() {
    let state = $scope.state
    if (state) {
      let exercise = state.routine
      $scope.timer_state = state
      $scope.interval = timer.start(parseInt(exercise.break), 0, function() {
        $interval.cancel($scope.interval)
        $scope.action = 'set'
      })
    } else {
      Speech.talk('Exercise complete!')
      $scope.action = 'stop'
    }
  }

  function stopTimer() {
    $interval.cancel($scope.interval)
  }

  function reset() {
    $scope.timeleft = 0
  }
  init();
}])
.filter('states', [ function() {
  return function(data) {
    if (data) {
      return data === "resume" ? 'Set' : data[0].toUpperCase() + data.slice(1);
    }
  }
}])
.filter('caltime', [ function() {
  return function(data) {
    let duration = parseInt(data.set_duration)
    let sets = parseInt(data.sets)
    let breaks = parseInt(data.break)
    let rest = parseInt(data.rest)

    return ((duration * sets) + (rest * (sets - 1)) + breaks) / 60;
  } 
}])
.filter('classes', [ function() {
  return function(data, param) {
    if (!param) {
      return data == "pause" ? "paused" : data;
    }

    return data == "pause" ? "timer-box paused" : "timer-box " + data;
  }
}])
.filter('setsFilter', [ function() {
  return function(data, param) {
    let result = (parseInt(param) - data)
    return Number.isNaN(result) ? '' : result;
  }
}])
.filter('cratefilter', [ function() {
  return function(data, param) {
    let crate = parseFloat(data)
    if (crate < 1 && crate > 0) {
      return '.' + crate.toString().split('.')[1]
    }

    return crate
  }
}])
