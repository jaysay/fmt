fmt_app.controller('TimerControls', [ '$scope', '$rootScope', '$interval', '$timeout', 'StateHandler', 'NTimer', 'Speech',
function($scope, $rootScope, $interval, $timeout, StateHandler, NTimer, Speech) {

  let timer = NTimer
  $scope.state = null;

  $rootScope.$watch('started', function(val) {
    if (val) {
      $scope.state = StateHandler.init($rootScope.routines)
      $scope.action = 'set';
    }
  })

  $scope.$watch('action', function(action) {
    if (action) {
      $scope.timer_next_state = action
      switch (action) {
        case 'set':
          $scope.state = StateHandler.set($scope.state);
          $scope.timer_state = $scope.state
          startSet()
          break
        case 'rest':
          $scope.state = StateHandler.rest($scope.state);
          $scope.timer_state = $scope.state
          $scope.disabled_btns = true;
          startRest()
          break
        case 'break':
          $scope.state = StateHandler.next($scope.state);
          if ($scope.state) { $scope.timer_state = $scope.state }
          $scope.disabled_btns = true;
          startBreak()
          break;
        case 'stop':
          stopTimer()
          break;
      }
    }
  })

  $scope.pause_timer = function() {
    $interval.cancel($scope.interval)
    $scope.timeleft = timer.lapsed
    $scope.timer_paused = true;
  }

  $scope.resume_timer = function() {
    $interval.cancel($scope.interval)
    let start_at = (timer.started_at - ($scope.timeleft / 1000).toFixed(2))
    let state = $scope.state
    let exercise = state.routine
    let next_action = state.remaining_sets === 0 && state.remaining_rest === 0 ? 'break' : 'rest'
    $scope.interval = timer.start(start_at, parseFloat(exercise.cycle_rate), function() {
      $interval.cancel($scope.interval)
      $scope.action = next_action
    })
    $scope.timer_paused = false;
  }

  $scope.replay_timer = function() {
    $interval.cancel($scope.interval)
    $scope.state = StateHandler.init($rootScope.routines)
    $scope.stop_timer()

    $scope.disabled_replay_btn = true;
    $scope.disabled_btns = true;
    $scope.disabled_stop = true;
    $scope.timer_paused = false;
    $timeout(function() {
      $scope.action = 'set'
    }, 500)

    $timeout(function() {
      $scope.disabled_replay_btn = false;
      $scope.disabled_btns = false;
      $scope.disabled_stop = false;
    }, 10000)
  }

  $scope.stop_timer = function() {
    $interval.cancel($scope.interval)
    timer.displayTime(0, 0)
    $scope.timer_paused = false;
    $scope.disabled_btns = true;
    $scope.action = 'stop'
  }

  function startSet () {
    $interval.cancel($scope.interval)
    reset()
    let state = $scope.state
    let exercise = state.routine
    let next_action = state.remaining_sets === 0 && state.remaining_rest === 0 ? 'break' : 'rest'
    let constraints = {
      throttle: state.is_next ? 3500 : 2500,
      message: state.is_next ? 'New exercise, ready, set ,start training' : 'Ready, set, start training',
    }

    $scope.disabled_btns = true;
    $scope.disabled_stop = true;
    $scope.disabled_replay_btn = true;
    Speech.talk(constraints.message, function() {
      $scope.interval = timer.start(parseInt(exercise.set_duration), parseFloat(exercise.cycle_rate), function() {
        $interval.cancel($scope.interval)
        $scope.action = next_action
      })
      $scope.disabled_btns = false;
      $scope.disabled_stop = false;
      $scope.disabled_replay_btn = false;
      $scope.$apply()
    })

    if (state.is_next) { state.is_next = false }
  }

  function startRest () {
    let state = $scope.state
    let exercise = state.routine
    $scope.interval = timer.start(parseInt(exercise.rest), 0, function() {
      $interval.cancel($scope.interval)
      $scope.action = 'set'
    })
  }

  function startBreak() {
    let state = $scope.state
    if (state) {
      let exercise = state.routine
      $scope.timer_state = state
      $scope.interval = timer.start(parseInt(exercise.break), 0, function() {
        $interval.cancel($scope.interval)
        $scope.action = 'set'
      })
    } else {
      Speech.talk('Exercise complete!')
      $scope.action = 'stop'
    }
  }

  function stopTimer() {
    $interval.cancel($scope.interval)
  }

  function reset() {
    $scope.timeleft = 0
  }
}])
